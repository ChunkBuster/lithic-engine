#pragma once
#include "fwd.h"

namespace lithic_engine {
    namespace globals {
        class Time {
        public:
            static void StepTime() {
                const double currentTime = glfwGetTime();
                _deltaTime = (currentTime - _lastTime);
                _lastTime = currentTime;
            }

            static double DeltaTime() {
                return _deltaTime;
            }

        private:
            static double _lastTime;
            static double _deltaTime;
        };

        class Environment {
            //public static Cubemap *environmentMap;
        };

        class Screen {
        public:
            static void SetScreenSize(glm::vec2 size) {
                _screenSize = size;
            }

            static glm::vec2 ScreenSize() {
                return _screenSize;
            }

            static glm::vec2 ScreenMiddle() {
                return _screenSize / 2.0f;
            }

        private:
            static glm::vec2 _screenSize;
        };
    }
}

