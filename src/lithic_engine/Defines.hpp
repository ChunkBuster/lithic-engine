﻿#pragma once
#include <vector>
#include <glm/detail/type_vec4.hpp>
#include <glm/detail/type_vec3.hpp>
#include <glm/detail/type_vec2.hpp>
#include <glm/detail/type_mat4x4.hpp>
#include <GL/glew.h>
#include <glm/common.hpp>
#include "Globals.hpp"

//Logging
#define LOG_ERROR_SYMBOL u8"✗"
#define LOG_MESSAGE_SYMBOL u8" "
#define LOG_WARNING_SYMBOL u8"△"
#define LOG_COMMENT_SYMBOL u8"♯"

enum LogType {
	Comment,
	Message,
	Error,
	Warning,
	Special
};
//resources
#define RESOURCE_CACHE_DIR "resources/cached/"
#define FLATBUF_MESH_EXTENSION ".meshbuf"
#define MESH_LOCATION 
#define TEXTURE_LOCATION

#define FONT_LOCATION "fonts/"
#define SHADER_LOCATION "shaders/"
#define SURFACE_SHADER_LOCATION SHADER_LOCATION + "surfaceShaders/"
#define COMPUTE_SHADER_LOCATION SHADER_LOCATION + "computeShaders/"
#define GEOMETRY_SHADER_LOCATION SHADER_LOCATION + "geometryShaders/"

#define COMPUTE_EXTENSION ".compute"
#define VERT_EXTENSION ".vert"
#define FRAG_EXTENSION ".frag"
#define GL_INCLUDE_EXTENSION ".glinc"
#define OBJ_MESH_EXTENSION ".obj"

//Defaults
#define WINDOW_WIDTH 1600
#define WINDOW_HEIGHT 1200
#define DEFAULT_SHADER "sparky"
#define DEFAULT_COMPUTE_SHADER "particle"
#define IMGUI_FONT_SIZE 22

//Libraries
#define IMGUI 1
#define IMGUI_INPUT 1
#define BULLET_PHYSICS 1

//Logging and debugging
#define VERBOSE_RESOURCES 0
#define CODE_CONTEXT_ERRORS 0
#define STDOUT_LOGGING 0
#define DEBUG_OBJECT_INSTANTIATION 1
#define PARTICLE_TEST 0
#define BALL_SHADER_TEST 0
#define DEBUG_OBJECT_DELETION 1

//Resource control
#define RESOURCE_CACHING 1
#define MESH_SPLITTING 0

//Rendering optimizations
#define FRUSTUM_CULLING 0
#define MAP_BOUNDS_RENDERING 0
#define DEBUG_BOUNDS_RENDERING false
#define INDIRECT_RENDERING 0
#define BOUNDS_RENDERING 0

//RenderDoc
#if DEBUG
#define RENDERDOC_INTEGRATION 0
#else
#define RENDERDOC_INTEGRATION 0
#endif

//Wallpaper Mode
#define WALLPAPER_MODE 1

#define TIME_CALL(x) \
  do { \
    	std::clock_t start; \
		double duration; \
		start = std::clock(); \
		x ;\
		duration = (std::clock() - start); \
		double durationSeconds = duration  / (double) CLOCKS_PER_SEC; \
		LOG(fmt::format("Time for \"(" #x ")\":{}ms ({}s).", duration, durationSeconds), LogType::Warning);\
  } while (0)

typedef unsigned int idxVal;
typedef std::vector<idxVal> idxVec;

inline std::istream &operator >>(std::istream &in, glm::vec2 &v) {
	in >> v.x;
	in >> v.y;
	return in;
}

inline std::istream &operator >>(std::istream &in, glm::vec3 &v) {
	in >> v.x;
	in >> v.y;
	in >> v.z;
	return in;
}

inline std::istream &operator >>(std::istream &in, glm::vec4 &v) {
	in >> v.x;
	in >> v.y;
	in >> v.z;
	in >> v.w;
	return in;
}

inline void glUniformColor(const GLint location, const glm::vec4 color) {
	glUniform4f(location, color.x, color.y, color.z, color.w);
}

inline void glUniformVec3(const GLint location, const glm::vec3 vec) {
	glUniform3f(location, vec.x, vec.y, vec.z);
}

inline float lerp_floats(const float a, const float b, float time) {
	time = glm::clamp(time, 0.0f, 1.0f);
	return a * (1 - time) + b * time;
}

inline glm::vec3 lerp_vec3(const glm::vec3 A, const glm::vec3 B, float t) {
	return A * t + B * (1.f - t);
}

inline glm::mat4 btScalar2glmMat4(btScalar *matrix) {
	return glm::mat4(
		matrix[0], matrix[1], matrix[2], matrix[3],
		matrix[4], matrix[5], matrix[6], matrix[7],
		matrix[8], matrix[9], matrix[10], matrix[11],
		matrix[12], matrix[13], matrix[14], matrix[15]);
}
