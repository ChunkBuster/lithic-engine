#pragma once
#include "src/lithic_engine/fwd.h"
#include "Transform.hpp"
#include "lithic_engine/debugging/Logger.hpp"

namespace lithic_engine
{
	/**
 * \brief Used to tell if a component added with AddComponent is, in fact, a component.
 */
#define IS_COMPONENT_ASSERT static_assert(std::is_base_of<Component, Comp>::value, "Comp must inherit from Component");

	typedef std::map<unsigned, std::unique_ptr<GameObject>> GameObjMap;
	typedef std::map<std::string, std::unique_ptr<Component>> ComponentMap;

	/**
	 * \brief The class which drives control of game logic. Can only be created with the Instantiate function.
	 */
	class GameObject {
		friend class Component; //Good buds
		/**
		 * \brief This ensures that the default constructor can't be called from other classes while
		 * retaining the ability to be made with std::make_unique.
		 */
		struct _privacyEnforcer { explicit _privacyEnforcer() = default; };
		
		public:
		GameObject(std::string name, _privacyEnforcer);
		~GameObject();

		static GameObject* Instantiate(std::string name);
		static void Destroy(GameObject*);

		static void UpdateAll();

		static void DisplayMenus();

		std::string GetName() const;

		/**
		 * \brief Adds a new component to this GameObject and returns a ref to it
		 * \tparam Comp Component type to add
		 * \tparam Args ctor argument types
		 * \param args arguments for ctor
		 * \return New component
		 */
		template <typename Comp, typename ...Args>
			Comp *AddComponent(const Args &... args) {
				IS_COMPONENT_ASSERT
					auto newComponent = _components.emplace(typeid(Comp).name(), std::make_unique<Comp>(this, args...));
				if(newComponent.second != true) {
					LOG(fmt::format("Could not attach component of type {} to {}, as one already existed. \
								Returning existing instance.", typeid(Comp).name(), _name), LogType::Warning);
					return nullptr;
				}
				return static_cast<Comp*>((*newComponent.first).second.get());
			}

		/**
		 * \brief Returns a ref to a component on this GameObject
		 * \tparam Comp Component type to get
		 * \return The component of type Comp on this GameObject
		 */
		template <typename Comp>
			Comp *GetComponent() {
				IS_COMPONENT_ASSERT
				std::string n = typeid(Comp).name();
				auto ptr = _components[n].get();
				if(!ptr)
					LOG(fmt::format("Could not find component of type {} on {}.", n, _name), LogType::Warning);
				return static_cast<Comp*>(ptr);
			}

		const std::unique_ptr<Transform> transform;


		private:
		void Update();
		void DisplayMenu();

		//todo: The destruction of this object at exit is likely the cause of the heap corruption issue
		//although it also might be related to something to do with gameobjects being destroyed so idk lol
		static std::unique_ptr<GameObjMap> AllGameObjects;
		
		static unsigned CurrentInstanceID;
		ComponentMap _components;
		std::string _name;
		int _instanceID;
	};
}
