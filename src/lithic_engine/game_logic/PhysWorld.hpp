#pragma once

namespace lithic_engine
{
	class PhysWorld {
	public:
		PhysWorld();
		~PhysWorld();
		static void register_rigidbody(btRigidBody *body);
		static void deregister_rigidbody(btRigidBody *body);
		static void register_renderer(Renderer *rend, btRigidBody *body);
		static void step();
	private:
		static PhysWorld *Instance;
		btBroadphaseInterface *broadphase;
		btDefaultCollisionConfiguration *collisionConfiguration;
		btCollisionDispatcher *dispatcher;
		btSequentialImpulseConstraintSolver *solver;
		btDiscreteDynamicsWorld *dynamicsWorld;
		std::map<btRigidBody*, Renderer*> rendererMap;
	};
}
