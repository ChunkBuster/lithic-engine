﻿#include "stdafx.h"
#include "lithic_engine/debugging/Logger.hpp"
#include "lithic_engine/game_logic/components/Renderer.hpp"
#include "Transform.hpp"
#include "PhysWorld.hpp"

namespace lithic_engine
{
	PhysWorld *PhysWorld::Instance = new PhysWorld();

	PhysWorld::PhysWorld() : broadphase(new btDbvtBroadphase()),
		collisionConfiguration(new btDefaultCollisionConfiguration()),
		dispatcher(new btCollisionDispatcher(collisionConfiguration)),
		solver(new btSequentialImpulseConstraintSolver) {
		//Mesh collisions
		btGImpactCollisionAlgorithm::registerAlgorithm(dispatcher);
		dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, broadphase, solver, collisionConfiguration);
		dynamicsWorld->setGravity(btVector3(0, -10, 0));
		LOG("Instantiated physics world", LogType::Message);
	}


	PhysWorld::~PhysWorld() {
		for (int i = Instance->dynamicsWorld->getNumCollisionObjects() - 1; i >= 0; i--)
		{
			btCollisionObject* obj = Instance->dynamicsWorld->getCollisionObjectArray()[i];
			Instance->dynamicsWorld->removeCollisionObject(obj);
			delete obj;
		}

		/*
		//delete collision shapes
		for (unsigned int j = 0; j < m_collisionShapes.size(); j++)
		{
			btCollisionShape* shape = m_collisionShapes[j];
			delete shape;
		}*/

		
		delete dynamicsWorld;
		delete solver;
		delete dispatcher;
		delete collisionConfiguration;
		delete broadphase;
	}


	void PhysWorld::register_rigidbody(btRigidBody *body) {
		Instance->dynamicsWorld->addRigidBody(body);
		LOG("Registered new rigidbody", LogType::Comment);
	}

	void PhysWorld::deregister_rigidbody(btRigidBody *body) {
		Instance->dynamicsWorld->removeRigidBody(body);
		LOG("Removed rigidbody", LogType::Comment);
	}

	void PhysWorld::register_renderer(Renderer *rend, btRigidBody *body) {
		Instance->rendererMap[body] = rend;
	}


	void PhysWorld::step() {
		Instance->dynamicsWorld->stepSimulation(globals::Time::DeltaTime(), 10);
	}
}
