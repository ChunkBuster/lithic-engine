#include "stdafx.h"
#include "Component.hpp"
#include "src/lithic_engine/Game.hpp"
#include "GameObject.hpp"
namespace lithic_engine
{
	Component::Component(GameObject *gameObj):transform(gameObj->transform.get())
	{
		_attachedObject = gameObj;
	}

	void Component::Update() { }

	void Component::DisplayMenu() { }

	GameObject* Component::AttachedObject() const {
		return _attachedObject;
	}
}
