#include "stdafx.h"
#include "GameObject.hpp"
#include "Component.hpp"
#include "src/lithic_engine/Game.hpp"

namespace lithic_engine
{
	std::unique_ptr<GameObjMap> GameObject::AllGameObjects(new GameObjMap());

	unsigned GameObject::CurrentInstanceID(0);

	//todo: Having CurrentInstanceID here is definitely the wrong move, it should be using the value passed in from the emplace  line below.
	GameObject::GameObject(std::string name, _privacyEnforcer enforcer) : transform(new Transform()), _instanceID(CurrentInstanceID) {
		_name = name;
	}
	
	GameObject* GameObject::Instantiate(std::string name) {
		CurrentInstanceID++;
		const auto end = AllGameObjects->find(CurrentInstanceID);
#if DEBUG_OBJECT_INSTANTIATION
		if (end != AllGameObjects->end())
			LOG(fmt::format("Tried to make an object of ID {} but it already existed!", CurrentInstanceID), Error);
		else
			LOG(fmt::format("Created object {} with ID {}", name, CurrentInstanceID), Message);
#endif
		auto newObj = AllGameObjects->emplace(CurrentInstanceID, std::make_unique<GameObject>(name, _privacyEnforcer{}));
		auto ptr = newObj.first->second.get();
		Game::AddGameObject(ptr);
		return ptr;
	}

	void GameObject::UpdateAll() {
		for(auto &g : *AllGameObjects) {
			if(g.second != nullptr)
				g.second->Update();
		}
	}

	void GameObject::DisplayMenus() {
		for (auto &g : *AllGameObjects) {
			if (g.second != nullptr)
				g.second->DisplayMenu();
		}
	}

	void GameObject::Destroy(GameObject* object) {
		if(object) {
			//(*AllGameObjects)[object->_instanceID].reset();
			const auto end = AllGameObjects->find(object->_instanceID);
			
			if(end != AllGameObjects->end())
			{
#if DEBUG_OBJECT_DELETION
				LOG(fmt::format("Destroyed {} (ID {})", object->_name, object->_instanceID), LogType::Message);
#endif
				AllGameObjects->erase(end);
			}	
			else
				LOG(fmt::format("Tried to destroy {} (ID {}) but it was already destroyed", object-> _name, object->_instanceID), LogType::Message);
			//std::unique_ptr<GameObject> ptr;
		}

	}

	std::string GameObject::GetName() const {
		return _name;
	}

	GameObject::~GameObject()
	= default;

	void GameObject::Update() {
		for (auto &c : _components) {
			c.second->Update();
		}
	}

	void GameObject::DisplayMenu() {
		for (auto &c : _components) {
			c.second->DisplayMenu();
		}
	}
}
