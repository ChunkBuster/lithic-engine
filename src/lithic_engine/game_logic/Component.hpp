#pragma once
#include "src/lithic_engine/fwd.h"

namespace lithic_engine
{
	/**
	 * \brief Components are essentially distinct logical units. Combined together on a GameObject,
	 * they can defined complex behaviors from their interactions with one another.
	 */
	class Component {	
	public:
		virtual ~Component() = default;
		Component(GameObject *gameObj);
		virtual void Update();
		virtual void DisplayMenu();
		/**
		 * \brief transform == the parent object's transform
		 */
		Transform *transform;
		GameObject* AttachedObject() const;

	protected:
		GameObject* _attachedObject = nullptr;
	};
}
