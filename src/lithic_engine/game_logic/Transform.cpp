﻿#include "stdafx.h"
#include "Transform.hpp"

#include "lithic_engine/debugging/Logger.hpp"
#include "lithic_engine/resources/Conversion.hpp"
namespace lithic_engine
{
	Transform::Transform(Transform *parent) : Transform() {
		this->parent = parent;
	}

	Transform::Transform() : parent(), scale(1), position(0), skew(0), perspective(0) {
	}

	glm::vec3 Transform::GlobalPosition() const {
		return glm::vec3(GetGlobalMatrix() * glm::vec4(0, 0, 0, 1));
	}

	glm::vec3 Transform::Up() const {
		return normalize(glm::vec3(0, 1, 0) * GetGlobalRotation());
	}

	glm::vec3 Transform::Down() const {
		return normalize(glm::vec3(0, -1, 0) * GetGlobalRotation());
	}

	glm::vec3 Transform::Right() const {
		return normalize(glm::vec3(1, 0, 0) * GetGlobalRotation());
	}

	glm::vec3 Transform::Back() const {
		return normalize(glm::vec3(0, 0, -1) * GetGlobalRotation());
	}

	glm::vec3 Transform::Left() const {
		return normalize(glm::vec3(-1, 0, 0) * GetGlobalRotation());
	}

	glm::vec3 Transform::Forward() const {
		return normalize(glm::vec3(0, 0, 1) * GetGlobalRotation());
		return normalize(glm::vec3(GetGlobalMatrix() * glm::vec4(0, 0, 1, 1)));
	}

	glm::mat4 Transform::GetMatrix() const {
		glm::mat4 outMat(1);
		outMat = translate(outMat, position);
		outMat *= toMat4(rotation);
		outMat = glm::scale(outMat, scale);
		return outMat;
	}

	glm::mat4 Transform::GetGlobalMatrix() const {
		glm::mat4 outMat(1);

		Transform *currParent = parent;
		std::vector<Transform *> parent_chain;
		while (currParent != nullptr) {
			parent_chain.push_back(currParent);
			currParent = currParent->parent;
		}

		unsigned parentChainSize = parent_chain.size();
		if(parentChainSize >= INT_MAX)
		{
			LOG(fmt::format("Transorm's parent chain is > {0}, too deep! Parent chains can only be {0} entries deep.", INT_MAX), Error);
		}
		for (int i = parent_chain.size() - 1; i >= 0; i--)
			outMat *= parent_chain[i]->GetMatrix();
		outMat *= GetMatrix();
		return outMat;
	}

	glm::quat Transform::GetGlobalRotation() const {
		glm::quat outQuat;

		Transform *currParent = parent;
		std::vector<Transform *> parent_chain;
		while (currParent != nullptr) {
			parent_chain.push_back(currParent);
			currParent = currParent->parent;
		}

		//for (int i = parent_chain.size() - 1; i >= 0; i--)
		//	outQuat *= parent_chain[i]->rotation;
		outQuat *= rotation;
		for (int i = 0; i < parent_chain.size(); i++)
			outQuat *= glm::inverse(parent_chain[i]->rotation);
		return outQuat;
	}

	void Transform::SetParent(Transform *newParent)
	{
		parent = newParent;
		__raise ChangedParent(newParent);
		__raise newParent->AddedChild(this);
	}
	
	Transform *Transform::GetParent()
	{
		return parent;
	}

	void Transform::SetMatrix(glm::mat4 matrix) {
		decompose(matrix, scale, rotation, position, skew, perspective);
	}
}