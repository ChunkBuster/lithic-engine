#pragma once
#include "src/lithic_engine/fwd.h"
#include "Transform.hpp"

namespace lithic_engine
{

	class InputManager {

	public:
		static ImGuiIO *io;

		static std::vector<glm::vec4> viewPlanes;
		static glm::vec3 position;
		static glm::vec2 sensitivity;
		static glm::vec2 moveSpeed;
		static Transform player_transform;

		static std::unique_ptr<GameObject> playerObject;

		static double xRot;
		static double yRot;

		static Weapon *player_gun;

		static void setCursorLock(GLFWwindow *window, bool lock);

		static void init_ui(GLFWwindow *window);
		static void process_input(GLFWwindow *window);
		static void draw_imgui();

		static boost::asio::io_service ioService;

	private:

	};

	void framebuffer_size_callback(GLFWwindow *window, int width, int height);
}