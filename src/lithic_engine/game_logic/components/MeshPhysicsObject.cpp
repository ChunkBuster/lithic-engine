#include "stdafx.h"
#include "MeshPhysicsObject.hpp"
#include "lithic_engine/game_logic/PhysWorld.hpp"
#include "lithic_engine/resources/utilities/Mesh.hpp"
#include "lithic_engine/game_logic/Transform.hpp"
#include "src/lithic_engine/Defines.hpp"

namespace lithic_engine
{
	MeshPhysicsObject::MeshPhysicsObject(GameObject *gameObj, std::shared_ptr<Mesh> model, bool isStatic) :
	PhysicsObject(gameObj, isStatic) {
		this->_model = model;
		if (isStatic)
			LoadStaticCollisionShape();
		else
			LoadDynamicCollisionShape();

	}


	MeshPhysicsObject::~MeshPhysicsObject() {
	}


	void MeshPhysicsObject::LoadStaticCollisionShape() {
		dynamic = false;
		_unoptimizedHull = nullptr;
		_hullOptimizer = nullptr;
		_hull = nullptr;

		_triangleMesh = new btTriangleMesh{};
		for (unsigned i{ 0 }; i < _model->indices.size(); i += 3) {
			_triangleMesh->addTriangle(
					btVector3(
						_model->vertices[_model->indices[i]].x,
						_model->vertices[_model->indices[i]].y,
						_model->vertices[_model->indices[i]].z),
					btVector3(
						_model->vertices[_model->indices[i + 1]].x,
						_model->vertices[_model->indices[i + 1]].y,
						_model->vertices[_model->indices[i + 1]].z),
					btVector3(
						_model->vertices[_model->indices[i + 2]].x,
						_model->vertices[_model->indices[i + 2]].y,
						_model->vertices[_model->indices[i + 2]].z)
					);
		}
		_triangleMeshShape = new btBvhTriangleMeshShape{ _triangleMesh, true };

		collisionState.reset(new btBvhTriangleMeshShape{ *_triangleMeshShape });
		glm::vec3 pos = this->transform->position;
		motionState = btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(pos.x, pos.y, pos.z)));
		const btRigidBody::btRigidBodyConstructionInfo groundRigidBodyCI(0, &motionState, collisionState.get(), btVector3(0, 0, 0));
		rb = new btRigidBody(groundRigidBodyCI);
		PhysWorld::register_rigidbody(rb);
	}

	void MeshPhysicsObject::LoadDynamicCollisionShape() {
		dynamic = true;
		_triangleMesh = nullptr;
		_triangleMeshShape = nullptr;

		_unoptimizedHull = new btConvexHullShape{};
		for (GLuint i : _model->indices) {
			btVector3 vertex{
				_model->vertices[i].x,
					_model->vertices[i].y, _model->vertices[i].z
			};
			_unoptimizedHull->addPoint(vertex);
		}

		_hullOptimizer = new btShapeHull{ _unoptimizedHull };
		btScalar margin{ _unoptimizedHull->getMargin() };
		_hullOptimizer->buildHull(margin);
		_hull = new btConvexHullShape{
			(btScalar*)_hullOptimizer->getVertexPointer(),
				_hullOptimizer->numVertices()
		};

		collisionState.reset(new btConvexHullShape{ *_hull });

		motionState = btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, 0, 0)));
		mass = 1;
		btVector3 fallInertia(0, 0, 0);
		collisionState->calculateLocalInertia(mass, fallInertia);
		const btRigidBody::btRigidBodyConstructionInfo fallRigidBodyCI(mass, &motionState, collisionState.get(), fallInertia);
		rb = new btRigidBody(fallRigidBodyCI);
		PhysWorld::register_rigidbody(rb);
	}
}
