#include "stdafx.h"
#include "Weapon.hpp"
#include "Renderer.hpp"
#include "lithic_engine/resources/utilities/Mesh.hpp"
#include "lithic_engine/game_logic/InputManager.hpp"
#include "lithic_engine/game_logic/PhysWorld.hpp"
#include "lithic_engine/game_logic/components/physics/PhysObj.hpp"
#include "lithic_engine/Game.hpp"
#include "lithic_engine/resources/Conversion.hpp"
#include "lithic_engine/game_logic/GameObject.hpp"
#include "lithic_engine/game_logic/components/physics/PhysSphere.hpp"
#include <glm/gtc/random.hpp>
#include <glm/gtx/color_space.hpp>
#include "lithic_engine/game_logic/components/physics/PhysBox.hpp"

namespace lithic_engine
{
	Weapon::Weapon(GameObject *object) :
	Component(object),
	rend(_attachedObject->AddComponent<Renderer>(Mesh::get_resource("Pistol", false))),
		properties(glm::vec3(0.18, -0.2, -0.3), "Ball"),
	
		_firing(false), _tip(*rend->transform) {
			rend->transform->scale = glm::vec3(1);
			rend->transform->rotation *= angleAxis(glm::radians(90.0f), glm::vec3(0, 1, 0));
			rend->transform->position = properties.start_pos;

			rend->transform->SetParent(&InputManager::player_transform);
			//rend->transf->position = glm::vec3(0, 0, 0.1f);
			_tip.SetParent(rend->transform);
			_tip.position = glm::vec3(0.3, 0, 0);
			_tip.rotation = glm::angleAxis(glm::radians(-90.0f), glm::vec3(0, 1, 0));
			LOG(fmt::format("Equipped Weapon \"Rifle\""), LogType::Message);

			rend->Enabled = false;
	}

	void Weapon::updateGunValues() const {
		static const GLuint lightUniformLocation = SurfaceShader::get_resource(DEFAULT_SHADER, false)->
			GetUniform("_LightPos");
		glUniform3fv(lightUniformLocation, 1, value_ptr(_tip.GlobalPosition()));
		static const GLuint lightPowerUniformLocation = SurfaceShader::get_resource(DEFAULT_SHADER, false)->
			GetUniform("_LightPower");
		glUniform1f(lightPowerUniformLocation, _brightness);
		static const GLuint lightColorUniformLocation = SurfaceShader::get_resource(DEFAULT_SHADER, false)->
			GetUniform("_LightColor");
		glUniform3fv(lightColorUniformLocation, 1, value_ptr(properties.color));
	};

	static std::vector<std::tuple<GameObject*, double>> balls;
	
	void Weapon::Update() 
	{
		//Handle ball destruction
		auto it = balls.begin();
		while (it != balls.end())
		{
			auto i = *it;
			if (glfwGetTime() - std::get<1>(i) > properties.bullet_life)
			{
				GameObject::Destroy(std::get<0>(i));
				it = balls.erase(it);
			}
			else
			{
				++it;
			}
		}
	}
	
	void Weapon::poll(int key, int action) {

 //Cut out because certain boost libs literally would not link no matter what
		if (key == GLFW_MOUSE_BUTTON_1) {

			if (action == GLFW_PRESS) {
				_firing = true;
				static bool firing_coroutine_running = false;
		
				if (!firing_coroutine_running) {
					
					spawn(InputManager::ioService, [&](boost::asio::yield_context yield) {
						firing_coroutine_running = true;
						double startTime = glfwGetTime();
						float scalar = 0;
						double deltaTime = 0;

						auto update_values = [&scalar, this]() {
							_brightness = lerp_floats(0, properties.brightness, scalar);
							rend->transform->position = lerp_vec3(properties.start_pos,
								properties.start_pos - glm::vec3(0, 0, properties.kickback), scalar);
							updateGunValues();
						};

						while (_firing) {
							deltaTime = glfwGetTime() - startTime;
							scalar = deltaTime / properties.fire_rate;
							if (scalar >= 1) {

								auto ballObj = GameObject::Instantiate("Ball");
								auto ballRend = ballObj->AddComponent<Renderer>(Mesh::get_resource("Ball"));

								float minSize = 0.5f;
								float maxSize = 0.5f;

								float radius = glm::linearRand(minSize, maxSize);

								ballRend->transform->scale = glm::vec3(radius);

								
								ballRend->opaqueSubmeshes[0].mat->diffus_color =
									glm::vec4(glm::rgbColor(glm::vec3((radius / (maxSize - minSize)) * 255.0,
										glm::linearRand(.4f, .5f), 1.0f)), 0.5);

								ballRend->opaqueSubmeshes[0].mat->diffus_color = glm::vec4(1, 1, 1, 1);

								//_tip.global_position()
								auto ballPhysObj = ballObj->AddComponent<PhysSphere>(glm::vec3(0), radius, false, 1);//
								//ball_obj->rb->(0.2);
								//auto ball_obj = ballObj->AddComponent<PhysBox>(glm::vec3(0), radius, false, 1);
								ballPhysObj->transform->position = _tip.GlobalPosition();
								ballPhysObj->reposition(glm_v3_to_bt(_tip.GlobalPosition()), glm_quat_to_bt(_tip.rotation));
								//todo: This ain't right, I should be able to get _tip.Forward() but that always points in one direction for some reason
								ballPhysObj->rb->applyCentralForce(glm_v3_to_bt(/*_tip.Forward()*/-InputManager::player_transform.Forward() * 1000.f));
								ballPhysObj->rb->setRestitution(0.5);
								startTime = glfwGetTime();
								scalar = 0;
								balls.push_back(std::make_tuple(ballObj, startTime));
							}
							//scalar = glm::mod(deltaTime / properties.fire_rate, 1.0);

							update_values();
							boost::asio::steady_timer t(InputManager::ioService, std::chrono::milliseconds(1));
							t.async_wait(yield);
						}
						
						while (scalar > 0) {
							boost::asio::steady_timer t(InputManager::ioService, std::chrono::milliseconds(1));
							t.async_wait(yield);
							scalar -= properties.fire_rate;
							update_values();
						}
						firing_coroutine_running = false;
						return;
					});
				}
			}

			if (action == GLFW_RELEASE) {
				_firing = false;
			}
		}
	}

	Weapon::~Weapon() {
	}
}
