#pragma once
#include "lithic_engine/game_logic/components/physics/PhysObj.hpp"

namespace lithic_engine
{
	class MeshPhysicsObject :
		public PhysicsObject {
	public:
		MeshPhysicsObject(GameObject *gameObj, std::shared_ptr<Mesh> model, bool isStatic);
		~MeshPhysicsObject();
		void LoadStaticCollisionShape();
		void LoadDynamicCollisionShape();
	private:
		std::shared_ptr<Mesh> _model;
		btConvexHullShape *_unoptimizedHull;
		btShapeHull *_hullOptimizer;
		btConvexHullShape *_hull;

		btTriangleMesh *_triangleMesh;
		btBvhTriangleMeshShape *_triangleMeshShape;

	};
}
