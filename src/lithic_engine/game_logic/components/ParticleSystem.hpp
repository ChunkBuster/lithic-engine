#pragma once
#include "lithic_engine/game_logic/Component.hpp"
#include "lithic_engine/resources/ComputeShader.hpp"

namespace lithic_engine
{
	enum ParticleMode {
		Environment = 1 << 0,	//Particles emitted randomly from within the bounding volume
		Sky			= 1 << 1,	//Particles emitted from the top of the bounding volume
		Point		= 1 << 2,	//Particles emitted from the center of the transform
		Cone		= 1 << 3,	//Particles emitted in a cone (not implemented)
	};

	#define PARTICLE_DATA_BIND 5
	class ParticleSystem : public lithic_engine::Component
	{
	public:
		~ParticleSystem();

		void Update() override;
		void DisplayMenu() override;

		void SetMaxCount(GLuint maxSize);

		void SetGravity(float gravity);
		void SetNoiseStrength(float strength);
		void SetTurbulence(float turbulence);

		void SetRate(glm::vec2 rate);
		void SetRate(float rate, float variance = 0);

		void SetLife(glm::vec2 rate);
		void SetLife(float life, float variance = 0);

		void SetScale(glm::vec2 scale);
		void SetScale(float scale, float variance = 0);

		void SetVolume(float x, float y, float z);
		void SetVolume(glm::vec3 volume);

		void SetCenter(float x, float y, float z);
		void SetCenter(glm::vec3 center);

		void UpdateUniforms();

		void SetMode(ParticleMode mode);

		ParticleSystem(GameObject *gameObj, GLuint maxSize, std::string name, bool startAlive = false, ParticleMode mode = Point);
		ParticleSystem(GameObject *gameObj, GLuint maxSize, std::string name, glm::vec3 pos, glm::vec3 volume, float initialSpeed, bool startAlive = false, ParticleMode mode = Point);
	private:
		
		struct particleInfo {
			glm::vec4 position;
			//Creation time, life time, lerp along life, slerp along life
			glm::vec4 timeState;
			//is alive, max scale, current scale, animation frame(not implemented)
			glm::vec4 state;
			glm::vec4 velocity;
		};

		GLuint _maxParticles = 1000;

		ParticleMode _mode;

		float _gravity = -0.001f;
		float _noiseStrength = 0.05;
		float _turbulence = 0.04;
		float _initialSpeed = 0;

		glm::vec2 _particleRate = glm::vec2(10, 0);
		glm::vec2 _particleLifetime = glm::vec2(1, 0.1);
		glm::vec2 _particleScale = glm::vec2(0.04f, 0.1);

		float _particleScaleVariation = 0.1f;
		float _startAlive = 1;

		glm::vec3 _lastPosition = glm::vec3(0);;

		glm::vec3 _volume = glm::vec3(30, 10, 15);
		glm::vec3 _center = glm::vec3(-10, 12, -5);

		std::shared_ptr<lithic_engine::ComputeShader> _computeShader;
		Renderer* _renderer;

		GLuint _dispatchedGroups[3];
	};
}

