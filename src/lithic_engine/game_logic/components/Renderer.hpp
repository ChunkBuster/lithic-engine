#pragma once
#include "lithic_engine/resources/SurfaceShader.hpp"
#include "lithic_engine/resources/utilities/Mesh.hpp"
#include "lithic_engine/utils/BoundingBox.hpp"
#include "lithic_engine/game_logic/Transform.hpp"

namespace lithic_engine
{
	class Transform;
	class Renderer : public Component {
	public:
		static void FinishRender();
		static std::unique_ptr<std::unordered_set<GLuint>> UsedShaders;

		Renderer(GameObject *gameObject, const std::shared_ptr<Mesh> &manager, bool renderBounds = false);
		~Renderer();

		void Draw();
		void RenderSubMesh(const Mesh::SubMesh& sub) const;
		void SetShader(std::string name);
		void SetShader(std::shared_ptr<SurfaceShader> manager);

		void SetDepthMode(GLboolean mode);
		GLboolean GetDepthMode() const;	

		std::vector<Mesh::SubMesh> opaqueSubmeshes;
		std::vector<Mesh::SubMesh> transpSubmeshes;

		void SetInstances(int instances, size_t size, void *data, GLuint bindingPoint);

		GLuint GetInstances() const;
		GLuint GetInstanceBuffer() const;

		bool Enabled = true;
		bool CullingEnabled = true;

	private:
		GLenum _drawMode = GL_FILL;
		GLboolean _depth = true;

		int _indexNumber;
		MeshBindInfo *_bindInfo = nullptr;
		std::shared_ptr<SurfaceShader> _shader = nullptr;
		std::shared_ptr<Mesh> _mesh = nullptr;

		idxVal _drawNumber = 1;
		GLuint _instanceBuffer = 0;
		GLuint _instances;

		bool BindTex(const std::shared_ptr<Material>& mat, MapType map, 
			const std::string &usingName, GLenum type = GL_TEXTURE_2D) const;
	};
}