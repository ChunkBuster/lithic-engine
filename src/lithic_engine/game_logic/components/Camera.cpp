#include <stdafx.h>
#include <src/lithic_engine/Globals.hpp>
#include "Camera.hpp"
namespace lithic_engine{

    Camera::Camera(GameObject *gameObj) : Component(gameObj), fov(80.0f) {

    }

    std::vector<glm::vec4> Camera::GetClippingPlanes() {
        std::vector<glm::vec4> viewPlanes;
        glm::mat4 m = projMatrix() * viewMatrix;
        //Pulled from GPU gems
        viewPlanes[ClipPlaneRight].x = m[0][3] + m[0][0];
        viewPlanes[ClipPlaneRight].y = m[1][3] + m[1][0];
        viewPlanes[ClipPlaneRight].z = m[2][3] + m[2][0];
        viewPlanes[ClipPlaneRight].w = m[3][3] + m[3][0];

        viewPlanes[ClipPlaneLeft].x = m[0][3] - m[0][0];
        viewPlanes[ClipPlaneLeft].y = m[1][3] - m[1][0];
        viewPlanes[ClipPlaneLeft].z = m[2][3] - m[2][0];
        viewPlanes[ClipPlaneLeft].w = m[3][3] - m[3][0];

        viewPlanes[ClipPlaneTop].x = m[0][3] - m[0][1];
        viewPlanes[ClipPlaneTop].y = m[1][3] - m[1][1];
        viewPlanes[ClipPlaneTop].z = m[2][3] - m[2][1];
        viewPlanes[ClipPlaneTop].w = m[3][3] - m[3][1];

        viewPlanes[ClipPlaneBottom].x = m[0][3] + m[0][1];
        viewPlanes[ClipPlaneBottom].y = m[1][3] + m[1][1];
        viewPlanes[ClipPlaneBottom].z = m[2][3] + m[2][1];
        viewPlanes[ClipPlaneBottom].w = m[3][3] + m[3][1];

        viewPlanes[ClipPlaneFar].x = m[0][2];
        viewPlanes[ClipPlaneFar].y = m[1][2];
        viewPlanes[ClipPlaneFar].z = m[2][2];
        viewPlanes[ClipPlaneFar].w = m[3][2];

        viewPlanes[ClipPlaneNear].x = m[0][3] - m[0][2];
        viewPlanes[ClipPlaneNear].y = m[1][3] - m[1][2];
        viewPlanes[ClipPlaneNear].z = m[2][3] - m[2][2];
        viewPlanes[ClipPlaneNear].w = m[3][3] - m[3][2];

        return viewPlanes;
    }

    glm::mat4 Camera::projMatrix() {
        return glm::perspective(glm::radians(fov), globals::Screen::ScreenSize().x / globals::Screen::ScreenSize().y, 0.1f, 100.0f);
    }

    Camera::~Camera() = default;
}