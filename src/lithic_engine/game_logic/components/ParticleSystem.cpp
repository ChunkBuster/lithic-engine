#include "stdafx.h"
#include "ParticleSystem.hpp"
#include "Renderer.hpp"
#include "src/lithic_engine/Game.hpp"
#include <imgui_internal.h>
#include "lithic_engine/game_logic/GameObject.hpp"

namespace lithic_engine
{
	ParticleSystem::ParticleSystem(GameObject *gameObj, GLuint maxSize, std::string name, glm::vec3 pos,
		glm::vec3 volume, float initialSpeed, bool startAlive, ParticleMode mode) :
		Component(gameObj),
		_computeShader(lithic_engine::ComputeShader::get_resource(DEFAULT_COMPUTE_SHADER)),
		_startAlive(startAlive),
		_renderer(_attachedObject->AddComponent<Renderer>(Mesh::get_resource(name))),
		_dispatchedGroups{ 1, 1, 1 }		
	{
		this->_initialSpeed = initialSpeed;
		SetMaxCount(maxSize);
		_renderer->SetShader(SurfaceShader::get_resource("particle"));
		_renderer->CullingEnabled = false;
		UpdateUniforms();
	}

	ParticleSystem::ParticleSystem(GameObject *gameObj, GLuint maxSize, std::string name, bool startAlive, ParticleMode mode) :
	ParticleSystem(gameObj, maxSize, name, glm::vec3(0), glm::vec3(0), 0, startAlive, mode)
	{
		
	}


	ParticleSystem::~ParticleSystem() {
		
	}

	void ParticleSystem::Update() {
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, _renderer->GetInstanceBuffer());
		glUseProgram(_computeShader->shaderProgram);
		glUniform1f(_computeShader->GetUniform("u_time"), float(glfwGetTime()));

		//Actually dispatch the compute shader
		glDispatchCompute(_dispatchedGroups[0], _dispatchedGroups[1], _dispatchedGroups[2]);

		glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
		//Force any following OpenGL commands to wait until the storage buffer(s) have been updated.
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	}

	void ParticleSystem::SetMaxCount(GLuint maxSize) {
		_maxParticles = maxSize;
		std::vector<particleInfo> instanceInfo;

		for (int i = 0; i < maxSize; i++) {
			auto randFloat = [](float low, float high) -> float {
				return low + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (high - low)));
			};
			instanceInfo.push_back({ 
				glm::vec4(randFloat(0, _volume.x), randFloat(0, _volume.y), randFloat(0, _volume.z), 0),
				glm::vec4(glfwGetTime(), _particleLifetime.x + randFloat(-_particleLifetime.y, _particleLifetime.y), 0, 0),
				glm::vec4(_startAlive,  _particleScale.x + randFloat(-_particleScale.y, _particleScale.y), 0, i),
				glm::vec4(
					randFloat(-_initialSpeed, _initialSpeed),
					randFloat(-_initialSpeed,_initialSpeed),
					randFloat(-_initialSpeed, _initialSpeed), 0) });

		}

		int wgs[3];
		
		glUseProgram(_computeShader->shaderProgram);
		glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 0, &wgs[0]);
		glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 1, &wgs[1]);
		glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 2, &wgs[2]);

		GLuint workGroupSize[3]{ GLuint(wgs[0]), GLuint(wgs[1]), GLuint(wgs[2]) };

		glUniform3fv(_computeShader->GetUniform("u_workSize"), 1, &(glm::vec3(workGroupSize[0], workGroupSize[1], workGroupSize[2]))[0]);
		_dispatchedGroups[0] = glm::clamp(maxSize, GLuint(0), workGroupSize[0]);
		if (maxSize > workGroupSize[0]) {
			maxSize -= workGroupSize[0];
			_dispatchedGroups[1] = glm::clamp(((maxSize / workGroupSize[0]) + 1), GLuint(0), workGroupSize[1]);
			if (maxSize > workGroupSize[0] * workGroupSize[1]) {
				maxSize -= workGroupSize[0] * (workGroupSize[1]);
				_dispatchedGroups[2] = glm::clamp((maxSize / (workGroupSize[0] * workGroupSize[1])) + 1, GLuint(0), workGroupSize[2]);
				if (maxSize >  workGroupSize[0] * workGroupSize[1] * workGroupSize[2]) {
					LOG("Attempted to spawn more compute workers than possible, somehow. Prepare for that crash, with your uncountable number of threads.", LogType::Warning);
				}

			}
		}
		_renderer->SetInstances(maxSize, sizeof(particleInfo), &instanceInfo[0], PARTICLE_DATA_BIND);
	}

	void ParticleSystem::SetGravity(float gravity) {
		_gravity = gravity;
	}

	void ParticleSystem::SetNoiseStrength(float noiseStrength) {
		_noiseStrength = noiseStrength;
	}

	void ParticleSystem::SetTurbulence(float turbulence) {
		_turbulence = turbulence;
	}

	void ParticleSystem::SetRate(glm::vec2 rate) {
		_particleRate = rate;
	}

	void ParticleSystem::SetRate(float rate, float variance) {
		SetRate(glm::vec2(rate, variance));
	}

	void ParticleSystem::SetLife(glm::vec2 rate) {
		_particleLifetime = rate;
	}

	void ParticleSystem::SetLife(float life, float variance) {
		SetLife(glm::vec2(life, variance));
	}

	void ParticleSystem::SetScale(glm::vec2 scale) {
		_particleScale = scale;
	}

	void ParticleSystem::SetScale(float scale, float variance) {
		SetScale(glm::vec2(scale, variance));
	}

	void ParticleSystem::SetVolume(float x, float y, float z) {
		SetVolume(glm::vec3(x, y, z));
	}

	void ParticleSystem::SetVolume(glm::vec3 volume) {
		_volume = volume;
	}


	void ParticleSystem::SetCenter(float x, float y, float z) {
		SetCenter(glm::vec3(x, y, z));
	}

	void ParticleSystem::SetCenter(glm::vec3 center) {
		_center = center;
	}

	void ParticleSystem::UpdateUniforms() {
		glUniform1f(_computeShader->GetUniform("u_noiseStrength"), _noiseStrength);
		glUniform1f(_computeShader->GetUniform("u_gravAccel"), _gravity);
		glUniform1f(_computeShader->GetUniform("u_coordNoiseMult"), _turbulence);
		glUniform2fv(_computeShader->GetUniform("u_particleScale"), 1, &_particleScale[0]);
		glUniform2fv(_computeShader->GetUniform("u_particleLifetime"), 1, &_particleLifetime[0]);
		glUniform3f(_computeShader->GetUniform("u_center"), _center.x, _center.y, _center.z);
		glUniform3f(_computeShader->GetUniform("u_bounds"), _volume.x, _volume.y, _volume.z);
	}

	void ParticleSystem::SetMode(ParticleMode mode) {
		_mode = mode;
	}

	void ParticleSystem::DisplayMenu() {
		if (ImGui::Begin("Particle Options", nullptr))
		{

			ImGui::SliderFloat("Gravity", &_gravity, -2, 2);
			ImGui::SliderFloat("NoiseStrength", &_noiseStrength, 0, 1);
			
			ImGui::SliderFloat("Turbulence", &_turbulence, 0, 1);
			ImGui::SliderFloat2("Scale", &_particleScale[0], 0, 0.3);
			ImGui::SliderFloat2("Lifetime", &_particleLifetime[0], 0, 20);
			ImGui::InputFloatN("Center", &_center[0], 3, 1, 0);
			ImGui::InputFloatN("Bounds", &_volume[0], 3, 1000, 0);
			if(ImGui::Button("Update")) {
				UpdateUniforms();
			}
			GLuint lastCount = _maxParticles;
			int max = static_cast<int>(_maxParticles);
			ImGui::InputInt("Max # of Particles", &max);
			if(max != lastCount) {
				SetMaxCount(max);
			}
		}
		ImGui::End();
	}
}
