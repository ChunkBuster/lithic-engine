#pragma once
#include "PhysObj.hpp"

namespace lithic_engine
{
	class PhysSphere : public PhysicsObject
	{
	public:
		PhysSphere(GameObject *gameObj, glm::vec3 origin, float radius, bool isStatic, float mass = 1);
		~PhysSphere();
	};
}
