#pragma once
#include "src/lithic_engine/fwd.h"
#include <LinearMath/btVector3.h>
#include "lithic_engine/game_logic/Component.hpp"

namespace lithic_engine
{
	class PhysicsObject : public Component{
	public:
		PhysicsObject(GameObject *gameObj, bool isStatic, float mass = 1);
		~PhysicsObject();


		bool dynamic;

		std::unique_ptr<btCollisionShape> collisionState;
		btDefaultMotionState motionState;
		btScalar mass;
		btVector3 inertia;
		btRigidBody *rb;

		void Update() override;
		void reposition(btVector3 position, btQuaternion orientation);
	};
}
