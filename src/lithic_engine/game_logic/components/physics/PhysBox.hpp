#pragma once
#include "PhysObj.hpp"

namespace lithic_engine
{
	class PhysBox : public PhysicsObject
	{
	public:
		PhysBox(GameObject *gameObj, glm::vec3 origin, float radius, bool isStatic, float mass = 1);
		~PhysBox();
	};
}
