#include "stdafx.h"
#include "PhysObj.hpp"
#include "lithic_engine/resources/utilities/Mesh.hpp"
#include "lithic_engine/game_logic/PhysWorld.hpp"
#include "lithic_engine/game_logic/Transform.hpp"
#include "lithic_engine/resources/Conversion.hpp"
namespace lithic_engine
{
	PhysicsObject::PhysicsObject(GameObject *gameObj, bool isStatic, float mass) : 
	Component(gameObj),
	dynamic(!isStatic), mass(mass), rb(nullptr) {

	}

	PhysicsObject::~PhysicsObject() {
		PhysWorld::deregister_rigidbody(rb);
	}

	void PhysicsObject::Update() {
		btTransform bt;
		glm::mat4 outMat(1);

		rb->getMotionState()->getWorldTransform(bt);

		const glm::quat rot = bt_quat_to_glm(bt.getRotation());
		const glm::vec3 pos = bt_v3_to_glm(bt.getOrigin());

		outMat = translate(outMat, pos);
		outMat *= glm::inverse(toMat4(rot));
		outMat = glm::scale(outMat, transform->scale);

		transform->SetMatrix(outMat);
	}

	void PhysicsObject::reposition(btVector3 position, btQuaternion orientation) {
		btTransform initialTransform;

		initialTransform.setOrigin(position);
		initialTransform.setRotation(orientation);

		rb->setWorldTransform(initialTransform);
		motionState.setWorldTransform(initialTransform);
	}
}