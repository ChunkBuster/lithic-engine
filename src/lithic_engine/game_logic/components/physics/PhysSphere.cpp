#include "stdafx.h"
#include "PhysSphere.hpp"
#include "lithic_engine/game_logic/PhysWorld.hpp"

namespace lithic_engine
{
	PhysSphere::PhysSphere(GameObject *gameObj, glm::vec3 origin, float radius, bool isStatic, float mass) : 
	PhysicsObject(gameObj, isStatic, mass)
	{
		dynamic = true;
		collisionState.reset(new btSphereShape(radius));
		btVector3 fallInertia(0, 0, 0);
		collisionState->calculateLocalInertia(mass, fallInertia);
		const btRigidBody::btRigidBodyConstructionInfo fallRigidBodyCI(mass, &motionState, collisionState.get(), fallInertia);
		rb = new btRigidBody(fallRigidBodyCI);
		rb->setFriction(10.0f);
		rb->setRestitution(0.5f);
		PhysWorld::register_rigidbody(rb);
	}


	PhysSphere::~PhysSphere()
	{
	}
}
