﻿#include "stdafx.h"

#include "lithic_engine/game_logic/InputManager.hpp"
#include "lithic_engine/game_logic/Transform.hpp"
#include "lithic_engine/resources/Material.hpp"
#include "lithic_engine/resources/Texture.hpp"
#include "lithic_engine/resources/Cubemap.hpp"
#include "lithic_engine/resources/SurfaceShader.hpp"
#include <boost/math/special_functions/sign.hpp>
#include "lithic_engine/rendering/IndirectRend.hpp"
#include "lithic_engine/resources/ComputeShader.hpp"
#include "src/lithic_engine/Game.hpp"
#include "lithic_engine/game_logic/GameObject.hpp"
#include "lithic_engine/game_logic/Component.hpp"

#include "Renderer.hpp"
#include "Camera.hpp"

namespace lithic_engine
{
	std::unique_ptr<std::unordered_set<GLuint>> Renderer::UsedShaders(new std::unordered_set<GLuint>());

	Renderer::Renderer(GameObject *gameObject, const std::shared_ptr<Mesh> &mesh, bool renderBounds) :
		Component(gameObject),
		_bindInfo(&mesh->GetBindingInfo()), _instances(0) {

			for (const auto &sub : mesh->subMeshes) {
				if (sub.mat->opacity == 1 || sub.mat->clip)
					opaqueSubmeshes.push_back(sub);
				else
					transpSubmeshes.push_back(sub);
			}
			_indexNumber = mesh->indices.size();
			_mesh = mesh;
			glGenBuffers(1, &_instanceBuffer);
			SetShader(DEFAULT_SHADER);
			//Do global binding work
#if INDIRECT_RENDERING
			IndirectRend::AddMesh(mesh);
#endif
			Game::AddRenderer(this);
		}

	void Renderer::SetShader(std::string name) {
		_shader = SurfaceShader::get_resource(name, false);
	}

	void Renderer::SetShader(std::shared_ptr<SurfaceShader> manager) {
		_shader = manager;
	}

	void Renderer::SetDepthMode(GLboolean mode) {
		_depth = mode;
	}

	GLboolean Renderer::GetDepthMode() const {
		return _depth;
	}


	void Renderer::FinishRender() {
		UsedShaders->clear();
	}

#define SPAWN_VARIANCE 20
	void Renderer::SetInstances(int instances, size_t size, void *data, GLuint bindingPoint) {
		_instances = instances;
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, _instanceBuffer);
		glBufferData(GL_SHADER_STORAGE_BUFFER, size * instances, data, GL_DYNAMIC_COPY);
		glBindBufferRange(GL_SHADER_STORAGE_BUFFER, bindingPoint, _instanceBuffer, 0, size * instances);
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0); //free him
	}

	GLuint Renderer::GetInstances() const {
		return _instances;
	}

	GLuint Renderer::GetInstanceBuffer() const {
		return _instanceBuffer;
	}

	Renderer::~Renderer() {
		glDeleteBuffers(1, &_instanceBuffer);
		Game::RemoveRenderer(this);
	}

	void Renderer::Draw() {
		if (!Enabled)
			return;
		glUseProgram(_shader->shaderProgram);
		//todo: This definitely shouldn't be here; Skyboxes should be map-dependent and not static, dog (same thing with the skybox lighting)
		static GLuint skybox = Cubemap::get_resource("Yokohama_HighRes", false)->texture;
		glActiveTexture(GL_TEXTURE0 + env_map);
		glBindTexture(GL_TEXTURE_CUBE_MAP, skybox);

		glm::mat4 proj = InputManager::playerObject->GetComponent<Camera>()->projMatrix();
		glm::mat4 view = InputManager::playerObject->GetComponent<Camera>()->viewMatrix;

		glm::mat4 model = transform->GetGlobalMatrix();

		glm::mat4 MV = view * model;
		glm::mat4 PV = proj * view;
		glm::mat4 MVP = proj * view * model;
		glUniformMatrix4fv(_shader->GetUniform("M"), 1, GL_FALSE, &model[0][0]);//&model[0][0]);
		//Update uniforms only a single time per shader per frame
		if(UsedShaders->find(_shader->shaderProgram) == UsedShaders->end()) {
			glUniformMatrix4fv(_shader->GetUniform("V"), 1, GL_FALSE, &view[0][0]);
			glUniformMatrix4fv(_shader->GetUniform("P"), 1, GL_FALSE, &proj[0][0]);
			glUniformMatrix4fv(_shader->GetUniform("PV"), 1, GL_FALSE, &PV[0][0]);
			glUniformMatrix4fv(_shader->GetUniform("MV"), 1, GL_FALSE, &MV[0][0]);
			glUniformMatrix4fv(_shader->GetUniform("MVP"), 1, GL_FALSE, &MVP[0][0]);
			//glUniform1f(_shader->GetUniform("u_time"), glfwGetTime());
			glUniformVec3(_shader->GetUniform("_CameraPosition"), InputManager::position);
			UsedShaders->insert(_shader->shaderProgram);
		}

		glBindVertexArray(_bindInfo->VAO);

		glDepthMask(_depth);

		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);

		/*
		   if (_depth) {
		   glEnable(GL_DEPTH_TEST);
		   glDepthFunc(GL_LEQUAL);

		   }
		   else {
		   glDisable(GL_DEPTH_TEST);
		   glDepthFunc(GL_ALWAYS);
		   }*/
		//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _bindInfo->SubMeshSuperEBO);

		for (const auto &sub : opaqueSubmeshes)
			RenderSubMesh(sub);

		glEnable(GL_BLEND);
		glDisable(GL_CULL_FACE);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		for (const auto &sub : transpSubmeshes)
			RenderSubMesh(sub);

		glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
		glBindVertexArray(0);
	}

	bool Renderer::BindTex(const std::shared_ptr<Material>& mat, MapType map, const std::string &usingName, GLenum type) const {
		if (mat->allMaps[map]) {
			glActiveTexture(GL_TEXTURE0 + map);
			glBindTexture(type, mat->allMaps[map]->texture);
			glUniform1f(_shader->GetUniform(usingName), 1);
			return true;
		}
		else {
			glUniform1f(_shader->GetUniform(usingName), 0);
			return false;
		}
	}

	void Renderer::RenderSubMesh(const Mesh::SubMesh& sub) const {

		static std::shared_ptr<SurfaceShader> skyboxShader = SurfaceShader::get_resource("skybox", false);

		glShadeModel(sub.smooth ? GL_SMOOTH : GL_FLAT);

#if FRUSTUM_CULLING
		if (CullingEnabled) {
			const glm::vec3 center = sub.bounds->GetTrueCenter() + transform->GlobalPosition();
			const glm::vec3 extent = sub.bounds->GetExtents();

			for (auto plane : InputManager::viewPlanes) {
				const glm::vec3 signVec(boost::math::sign(plane.x), boost::math::sign(plane.y), boost::math::sign<float>(plane.z));
				if (glm::dot(center + extent * signVec, glm::vec3(plane)) < -plane.w)
					return;
			}
		}
#endif

		static std::shared_ptr<Material> lastMaterial = nullptr;
		//if (lastMaterial != sub.mat)
		{
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, sub.EBO);

			//glUniformColor(_shader->GetUniform("u_AlbedoColor"), sub.mat->diffus_color * glm::vec4(1, 1, 1, sub.mat->opacity));
			glUniformColor(_shader->GetUniform("u_AlbedoColor"), glm::vec4(1, 1, 1, 1));
			BindTex(sub.mat, diffus_map, "u_UsingAlbedoMap");
			if(!BindTex(sub.mat, spec_map, "u_UsingSpecularMap")) 
				glUniformVec3(_shader->GetUniform("u_SpecularColor"), sub.mat->spec_color);

			BindTex(sub.mat, normal_map, "u_UsingNormalMap");
			BindTex(sub.mat, clip_map, "u_UsingClipMap");
			if(!BindTex(sub.mat, roughness_map, "u_UsingRoughnessMap"))
				glUniform1f(_shader->GetUniform("u_RoughnessColor"), sub.mat->roughness);

			/*
			const bool rough = sub.mat->allMaps[roughness_map] != nullptr;
			const bool gloss = sub.mat->allMaps[gloss_map] != nullptr;

			if ( rough || gloss) {
				
				glActiveTexture(GL_TEXTURE0 + roughness_map);
				if (rough) {
					glBindTexture(GL_TEXTURE_2D, sub.mat->allMaps[roughness_map]->texture);
				}
				else if (gloss) {
					glBindTexture(GL_TEXTURE_2D, sub.mat->allMaps[gloss_map]->texture);
				}
				if (rough && gloss)
					LOG(fmt::format("Roughness and Gloss maps both set for a submesh on {}!", _mesh->GetName()), LogType::Error);
			}else {
				glUniform1f(_shader->GetUniform("u_RoughnessColor"), sub.mat->roughness);
			}

			const bool usingRough = rough;

			glUniform1f(_shader->GetUniform("u_UsingRoughnessMap"), usingRough);
			glUniform1f(_shader->GetUniform("u_UsingGlossMap"), !usingRough);
			*/
			if (_drawMode == GL_LINE) {
				glUniformColor(_shader->GetUniform("u_AlbedoColor"), glm::vec4(0, 0, 0, 1));
				glUniform1f(_shader->GetUniform("u_UsingAlbedoMap"), 0);
				glLineWidth(2.f);
			}
			else {
				glEnable(GL_CULL_FACE);
			}
		}

		glPolygonMode(GL_FRONT_AND_BACK, _drawMode);
		if(_instances > 0)
			glDrawElementsInstanced(GL_TRIANGLES, sub.indices.size(), GL_UNSIGNED_INT, nullptr, _instances);
		else
			glDrawElements(GL_TRIANGLES, sub.indices.size(), GL_UNSIGNED_INT, nullptr);
		lastMaterial = sub.mat;
	}

}
