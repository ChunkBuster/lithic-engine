#pragma once
#include <src/lithic_engine/game_logic/Component.hpp>

namespace lithic_engine{
    class Camera : public Component  {
    public:
        enum FrustumPlane {
            ClipPlaneRight,
            ClipPlaneLeft,
            ClipPlaneTop,
            ClipPlaneBottom,
            ClipPlaneFar,
            ClipPlaneNear
        };

        explicit Camera(GameObject *gameObj);
        ~Camera();

        glm::mat4 viewMatrix;
        glm::mat4 projMatrix();

        float fov;
        std::vector<glm::vec4> GetClippingPlanes();
    };
}
