#pragma once
#include "src/lithic_engine/fwd.h"
#include "lithic_engine/game_logic/Transform.hpp"
#include "lithic_engine/resources/utilities/Mesh.hpp"

namespace lithic_engine
{
	class Weapon : public Component{
	public:
		Weapon(GameObject *gameObject);
		~Weapon();

		void poll(int key, int action);
		Renderer *rend;

		struct weapon_properties {
			float fire_rate = 0.1f;
			float bullet_life = 3.0f;
			float brightness = 2.0f;
			glm::vec3 color = { 1, 0.4f, 0.1 };
			float kickback = 0.03f;
			glm::vec3 start_pos;
			std::shared_ptr<Mesh> projectile;
			weapon_properties(glm::vec3 startPos, std::string projectileName) : 
				start_pos(startPos), projectile(Mesh::get_resource(projectileName, false)) {}
		};

		weapon_properties properties;
		void Update();
	private:
		bool _firing;
		Transform _tip;
		void updateGunValues() const;
		float _brightness = 0;
		
	};
}