﻿#pragma once
#include "src/lithic_engine/fwd.h"
namespace lithic_engine
{
	class Transform {
	public:
		Transform();
		explicit Transform(Transform *parent);
		
		void SetParent(Transform *newparent);
		Transform *GetParent();

		glm::vec3 scale;
		glm::quat rotation;
		glm::vec3 position;
		glm::vec3 skew;
		glm::vec4 perspective;

		glm::vec3 GlobalPosition() const;

		glm::vec3 Up() const;
		glm::vec3 Right() const;
		glm::vec3 Down() const;
		glm::vec3 Left() const;
		glm::vec3 Forward() const;
		glm::vec3 Back() const;

		glm::mat4 GetMatrix() const;
		glm::mat4 GetGlobalMatrix() const;
		glm::quat GetGlobalRotation() const;

		void SetMatrix(glm::mat4 matrix);
		
		__event void AddedChild(Transform *newChild);
		__event void ChangedParent(Transform *newParent);

	private:
		Transform *parent;
	};
}