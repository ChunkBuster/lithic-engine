#include "stdafx.h"
#include "Globals.hpp"
namespace lithic_engine{
    double globals::Time::_lastTime(0);
    double globals::Time::_deltaTime(0);
    glm::vec2 globals::Screen::_screenSize(0);
}
