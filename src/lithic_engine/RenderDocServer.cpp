﻿#include "stdafx.h"
#if RENDERDOC_INTEGRATION
#include "RenderDocServer.hpp"
#include "Logger.hpp"

std::unique_ptr<RenderDocServer> RenderDocServer::_instance(nullptr);

bool RenderDocServer::Loaded() {
	return _instance && _instance->rdoc_api;
}
//This would just be a static initialization but RenderDoc screams when you do that
void RenderDocServer::Init() {
	assert(!_instance);
	_instance.reset(new RenderDocServer());
}

RenderDocServer::RenderDocServer() {
#if _WIN32 || _WIN64
	if ((rdoc_mod = LoadLibrary(RENDERDOC_LIB)))
#else 
	if ((_instance->rdoc_mod = dlopen(RENDERDOC_LIB)))
#endif
	{
		const auto RENDERDOC_GetAPI = reinterpret_cast<pRENDERDOC_GetAPI>(
#if _WIN32 || _WIN64
			GetProcAddress(rdoc_mod, "RENDERDOC_GetAPI"));
#else
			dlsym(rdoc_mod, "RENDERDOC_GetAPI"));
#endif

		assert(RENDERDOC_GetAPI(eRENDERDOC_API_Version_1_1_2, reinterpret_cast<void **>(&rdoc_api)));
		rdoc_api->SetCaptureKeys(nullptr, 0); //Remove default (broken) capture key, use Capture().

		if(!(RDOC_DEFAULT_SETTINGS)) 
			LOG("RenderDoc config failed", LogType::Error);
		
		int version[3];
		rdoc_api->GetAPIVersion(&version[0], &version[1], &version[2]);
		LOG(fmt::format("RenderDoc v{}.{}.{} Initialized", version[0], version[1], version[2]), LogType::Special);
	}
	else {
		const auto error = GetLastError();
		LOG(fmt::format("RenderDoc: Could not load " RENDERDOC_LIB ", {}", error), LogType::Error);
	}
}

void RenderDocServer::AssignWindow(void *deviceHandle, void* windowHandle) {
	if (Loaded())
		_instance->rdoc_api->SetActiveWindow(deviceHandle, windowHandle);
	else
		LOG("Attempted to assign window, but RenderDoc was not loaded!", LogType::Warning);
}

void RenderDocServer::Close() {
	if (Loaded() && _instance->rdoc_mod) {
		_instance->rdoc_api->UnloadCrashHandler();
#if _WIN32 || _WIN64
		FreeLibrary(_instance->rdoc_mod);
#else 
		dlclose(_instance->rdoc_mod);
#endif
	}
}
		
void RenderDocServer::Capture() {
	if (Loaded()) {
		_instance->rdoc_api->TriggerCapture();
		LOG("Snapshot captured", LogType::Special);
		if (!_instance->rdoc_api->IsTargetControlConnected())
			_instance->rdoc_api->LaunchReplayUI(1, nullptr);
	}
	else {
		LOG("Attempted to capture, but RenderDoc was not loaded", LogType::Warning);
	}
}
#endif
