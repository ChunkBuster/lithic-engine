#pragma once
#include "src/lithic_engine/fwd.h"
#include "lithic_engine/resources/ResourceManager.hpp"
namespace lithic_engine
{
	
	/**
	 * \brief These correlate with binding points in the shaders.
	 */
	enum MapType {
		no_map,
		diffus_map,
		spec_map,
		roughness_map,
		normal_map,
		clip_map,
		env_map,
		preint_fg,
		shadow_map ,
		amb_map ,
		emissive_map ,
		gloss_map ,
		bump_map ,
		stencil_map ,
		anisotropy_map, 
		num_maps
	};

	/**
	 * \brief A class use to store properties used by renderers for rendering
	 */
	class Material : public ResourceManager<Material> {
	public:
		explicit Material(std::string name);
		~Material();
		//Color attributes
		glm::vec4 amb_color = glm::vec4(0);
		glm::vec4 diffus_color = glm::vec4(1);
		glm::vec3 spec_color = glm::vec3(0);
		glm::vec4 emissive_color = glm::vec4(0);

		//Scalar attributes
		float spec_exponent = 1;
		float opacity = 1;
		float anisotropy = 0;
		float roughness = 1;

		//Special attributes
		int illum = 0;
		bool clip = false;

		//Various texture channels
		std::vector<std::shared_ptr<Texture>> allMaps;
		

		std::string dir() override;
	protected:
		bool loadResource(std::string name, bool mt) override;
	};
}