#pragma once
#include "lithic_engine/resources/ResourceManager.hpp"
namespace lithic_engine
{
	class SurfaceShader : public ResourceManager<SurfaceShader> {
	public:
		explicit SurfaceShader(std::string name);
		~SurfaceShader();

		typedef std::map<std::string, SurfaceShader*> ShaderMap;
		unsigned int shaderProgram;

		/**
		 * \brief Returns the uniform bound at a given string
		 * \param name Name of the uniform to find
		 * \return The uniform bound to that name
		 */
		GLuint GetUniform(std::string name);
		std::string dir() override;
		/**
		 * \brief In-place recompile of shaders
		 */
		static void RecompileShaders();
	protected:
		bool loadResource(std::string name, bool mt) override;

	private:
		/**
		 * \brief Contains all pre-indexed Uniforms for this shader
		 */
		std::map<std::string, GLuint> uniforms;

		//Binding points for the vert and frag shaders
		GLuint _vertexShader;
		GLuint _fragmentShader;
	};
}
