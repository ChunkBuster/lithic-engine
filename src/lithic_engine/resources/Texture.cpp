﻿#include "stdafx.h"
#include "Texture.hpp"
// Include GLEW
#include <GL/glew.h>


namespace lithic_engine
{
    Texture::Texture(std::string name) :
        ResourceManager<Texture>(name), texture(0), width(0), height(0), channels(0) {
    }


    std::string Texture::dir() {
        return ResourceManager<Texture>::dir() + "textures/";
    }

    bool Texture::loadResource(std::string name, bool mt) {
        std::string location = dir() + name;

        //Create and load textures to OpenGL 
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);


		//Load the image data
		unsigned char* imageData = stbi_load(location.c_str(), &width, &height, &channels, STBI_rgb_alpha);

		if (!imageData) {
			LOG(fmt::format("Failed to texture {}", name), LogType::Error);
			stbi_image_free(imageData);
			return false;
		}
        //This is a bit of a hack, but don't generate mip maps for clip masks
        bool shouldGenerateMips = name.find("_mask") == std::string::npos;

        if(shouldGenerateMips) {
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 5);
        }

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width,
            height, 0, GL_RGBA, GL_UNSIGNED_BYTE,
			imageData);

        //glTexStorage2D(GL_TEXTURE_2D, 5, format, width, height);
        //glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, rawImageData);

        if (shouldGenerateMips) {
            glGenerateMipmap(GL_TEXTURE_2D);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        }else {
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        }
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		stbi_image_free(imageData);
        //glHint(GL_GENERATE_MIPMAP_HINT, GL_NICEST);
        //glGenerateMipmap(GL_TEXTURE_2D);
#if VERBOSE_RESOURCES
        LOG(fmt::format("Loaded image {} ({}px by {}px, {}channels)", name, width, height, channels), LogType::Comment, u8"🎨");
#endif
        loaded = true;
        return true;
    }

    Texture::~Texture() {
    }

}