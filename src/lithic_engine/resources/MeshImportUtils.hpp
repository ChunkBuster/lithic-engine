#pragma once
#include "src/lithic_engine/Defines.hpp"
#include <map>
#include <unordered_set>
#include <iostream>
#include <iomanip>
#include <functional>
#include <string>
#include <unordered_set>
namespace lithic_engine
{
	namespace import_utils
	{
#define APPROX_FUDGE 0.001f
		inline bool approx(const float x, const float y) {
			return fabs(x - y) < APPROX_FUDGE;
		}

		inline bool approx(const glm::vec3 &x, const glm::vec3 &y) {
			bool val = approx(x.x, y.x);
			val &= approx(x.y, y.y);
			val &= approx(x.z, y.z);
			return val;
		}

		/**
		* \brief Allows vec3s to be used as keys in a dictionary and unsorted set
		*/
		struct VertKey
		{
			glm::vec3 vert;
			bool operator==(const VertKey &other) const
			{
				return approx(vert, other.vert);
			}
			bool operator<(const VertKey that) const
			{
				auto val = memcmp((void*)this, (void*)(&that), sizeof(VertKey));
				return val > 0;
			}
			VertKey(glm::vec3 vert) : vert(vert) {}
		};


		class  compareKeyIndex : public std::binary_function<VertKey, VertKey, bool>
		{
		public:
			//Epsilon for squared vals??
			double eps;
			compareKeyIndex(double arg_ = 1e-7) : eps(arg_) {}
			bool operator()(const VertKey &left, const VertKey &right) const
			{
				return abs(glm::length2(left.vert - right.vert)) > APPROX_FUDGE && (glm::length2(left.vert) < glm::length2(right.vert));
			}
		};

	}
}
namespace std
{
	template<>
	struct hash<glm::vec3>
	{
		size_t operator()(const glm::vec3 & obj) const
		{
			if (memcmp((void*)this, (void*)(&obj), sizeof(glm::vec3)) == 0)
				return false;
			size_t h = hash<int>()(obj.x);
			h ^= hash<int>()(obj.y);
			h ^= hash<int>()(obj.z);
			return h;
		}
	};

	template<>
	struct hash<lithic_engine::import_utils::VertKey>
	{
		size_t operator()(const lithic_engine::import_utils::VertKey & obj) const
		{
			return hash<glm::vec3>{}(obj.vert);
		}
	};
}
namespace lithic_engine
{
	namespace import_utils
	{

		struct glmKeyFuncs
		{
			size_t operator()(const glm::vec1 &k) const {
				return std::hash<float>()(k.x);
			}
			bool operator()(const glm::vec1 &a, const glm::vec1 &b) const {
				return approx(a.x, b.x);
			}
			size_t operator()(const glm::vec2 &k) const {
				return std::hash<float>()(k.x) ^ std::hash<float>()(k.y);
			}
			bool operator()(const glm::vec2 &a, const glm::vec2 &b) const {
				return approx(a.x, b.x) && approx(a.y, b.y);
			}
			size_t operator()(const glm::vec3 &k) const {
				return std::hash<float>()(k.x) ^ std::hash<float>()(k.y) ^ std::hash<int>()(k.z);
			}
			bool operator()(const glm::vec3 &a, const glm::vec3 &b) const {
				return approx(a.x, b.x) && approx(a.y, b.y) && approx(a.z, b.z);
			}
			size_t operator()(const glm::vec4 &k) const {
				return std::hash<float>()(k.x) ^ std::hash<int>()(k.y) ^ std::hash<float>()(k.z) ^ std::hash<float>()(k.w);
			}
			bool operator()(const glm::vec4 &a, const glm::vec4 &b) const {
				return approx(a.x, b.x) && approx(a.y, b.y) && approx(a.z, b.z) && approx(a.w, b.w);
			}
		};

		struct VertIndexPairKeyFuncs {
			size_t operator()(const VertKey &k) const {
				const glm::vec3 toHash = k.vert;
				return std::hash<glm::vec3>{}(toHash);
			}
			bool operator()(const VertKey &a, const VertKey &b) const {
				return approx(a.vert, b.vert);
			}
		};

		typedef std::map<VertKey, std::unordered_set<VertKey>, compareKeyIndex> VertNeighborMap;
		typedef std::map<glm::vec3, glm::vec3, glmKeyFuncs, glmKeyFuncs> vec3Map;
		typedef std::map<glm::vec2, glm::vec2, glmKeyFuncs, glmKeyFuncs> vec2Map;



		struct MaterialAssocData {
			std::vector<glm::vec3> positions;
			std::vector<glm::vec2> uvs;
			std::vector<glm::vec3> normals;
		};

		struct IndexGroup {
			idxVec vertIndices;
			idxVec uvIndices;
			idxVec normalIndices;
			idxVec initialIndices;
			idxVec finalIndices;
			std::vector<idxVec> finalIndicesSplit;
			VertNeighborMap neighbors;

			IndexGroup() {
			}

			IndexGroup(idxVec &vertIndices, idxVec &uvIndices, idxVec &normalIndices)
				: vertIndices(vertIndices), uvIndices(uvIndices), normalIndices(normalIndices) {
			}
		};

		struct FatPackedVertex {
			glm::vec3 position;
			glm::vec2 uv;
			glm::vec3 normal;

			bool operator<(const FatPackedVertex that) const {
				return memcmp((void*)this, (void*)&that, sizeof(FatPackedVertex)) > 0;
			};
		};

		struct PackedVertex {
			glm::vec3 position;

			bool operator<(const PackedVertex that) const {
				return memcmp((void*)this, (void*)&that, sizeof(PackedVertex)) > 0;
			};
		};

#pragma region This block of code was largely inspired by https://github.com/huamulan/OpenGL-tutorial/blob/master/common/vboindexer.cpp
		inline bool getSimilarFatVert(FatPackedVertex &packed, std::map<FatPackedVertex, idxVal> &VertexToOutIndex, idxVal &result) {
			std::map<FatPackedVertex, idxVal>::iterator it = VertexToOutIndex.find(packed);
			if (it == VertexToOutIndex.end()) {
				return false;
			}
			result = it->second;
			return true;
		}

		inline bool getSimilarVert(PackedVertex &packed, std::map<PackedVertex, idxVal> &VertexToOutIndex, idxVal &result) {
			std::map<PackedVertex, idxVal>::iterator it = VertexToOutIndex.find(packed);
			if (it == VertexToOutIndex.end()) {
				return false;
			}
			result = it->second;
			return true;
		}

		inline std::map<FatPackedVertex, idxVal> indexVBO(
			std::vector<glm::vec3> &in_vertices,
			std::vector<glm::vec2> &in_uvs,
			std::vector<glm::vec3> &in_normals,
			std::vector<glm::vec3> &in_tangents,
			std::vector<glm::vec3> &in_bitangents,
			idxVec &in_indices			
		) {
			std::vector<glm::vec3> out_vertices;
			std::vector<glm::vec2> out_uvs;
			std::vector<glm::vec3> out_normals;
			std::vector<glm::vec3> out_tangents;
			std::vector<glm::vec3> out_bitangents;
			std::map<FatPackedVertex, idxVal> VertIndexMap;
			idxVec out_indices;

			for (unsigned int i = 0; i < in_vertices.size(); i++) {
				FatPackedVertex fatPackedVertex = { in_vertices[i], in_uvs[i], in_normals[i] };
				idxVal indexFat;
				if (!getSimilarFatVert(fatPackedVertex, VertIndexMap, indexFat)) {
					out_vertices.push_back(in_vertices[i]);
					out_uvs.push_back(in_uvs[i]);
					out_normals.push_back(in_normals[i]);
					out_tangents.push_back(in_tangents[i]);
					out_bitangents.push_back(in_bitangents[i]);
					//Assign the new index
					indexFat = static_cast<idxVal>(out_vertices.size() - 1);
					VertIndexMap[fatPackedVertex] = indexFat;
				}
				else {
					out_tangents[indexFat] += in_tangents[i];
					out_bitangents[indexFat] += in_bitangents[i];
				}
				out_indices.push_back(indexFat);
			}

			//Now that the new values have been populated, swap out the pointers and remove the old versions
			in_vertices = out_vertices;
			in_uvs = out_uvs;
			in_normals = out_normals;
			in_tangents = out_tangents;
			in_bitangents = out_bitangents;
			in_indices = out_indices;
			return VertIndexMap;
		}
#pragma endregion
	}
}