﻿#include "stdafx.h"
#include "SurfaceShader.hpp"
#include "lithic_engine/resources/Texture.hpp"
#include "lithic_engine/resources/Material.hpp"
#include "lithic_engine/Defines.hpp"
#include "lithic_engine/resources/utilities/ShaderImportUtils.hpp"

namespace lithic_engine
{
	SurfaceShader::SurfaceShader(std::string name) :
		ResourceManager<SurfaceShader>(name),
		shaderProgram(glCreateProgram()) {
	}

	
	bool SurfaceShader::loadResource(std::string name, bool mt) {
		const std::string vertLocation = dir() + name + VERT_EXTENSION;
		const std::string fragLocation = dir() + name + FRAG_EXTENSION;

		if (!CompileShader(_vertexShader, vertLocation, Name, GL_VERTEX_SHADER))
			return false;
		if (!CompileShader(_fragmentShader, fragLocation, Name, GL_FRAGMENT_SHADER))
			return false;
		if (!LinkShader(shaderProgram, Name, _vertexShader, _fragmentShader) == GL_TRUE)
			return false;
		return true;
	}

	std::string SurfaceShader::dir() {
		return ResourceManager<SurfaceShader>::dir() + SURFACE_SHADER_LOCATION;
	}

	void SurfaceShader::RecompileShaders() {
		for (auto &pair : ResourceMap) {
			SurfaceShader *manager = ResourceMap[pair.first].get();
			glDetachShader(manager->shaderProgram, manager->_vertexShader);
			glDetachShader(manager->shaderProgram, manager->_fragmentShader);
			manager->uniforms.clear();
			manager->loadResource(pair.first, false);
		}
	}

	SurfaceShader::~SurfaceShader() {
	}

	GLuint SurfaceShader::GetUniform(std::string uniform_name) {
		GLuint uniformLocation = uniforms[uniform_name];
		if (!uniformLocation) {
			uniformLocation = glGetUniformLocation(shaderProgram, uniform_name.c_str());
			if (uniformLocation == GL_INVALID_VALUE)
				LOG(fmt::format("Attempted to get uniform \"{}\" from shader {} , but it was not found in the program", uniform_name,
					Name), LogType::Warning);
			uniforms[uniform_name] = uniformLocation;
		}
		return uniformLocation;
	}
}