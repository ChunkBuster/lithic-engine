#pragma once
#include "ResourceManager.hpp"
namespace lithic_engine
{
	class Cubemap : public ResourceManager<Cubemap> {
	public:
		typedef std::map<std::string, Texture*> ImageMap;

		explicit Cubemap(std::string name);
		~Cubemap();

		unsigned int texture;

		int width;
		int height;
		int channels;
		GLuint format;

	protected:
		//void parse_image(unsigned char *imageData);
		std::string dir() override;
		bool loadResource(std::string name, bool mt) override;

	private:
		bool initialized = false;
		std::vector<Texture *> _textures;
	};
}
