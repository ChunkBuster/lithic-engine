#pragma once
inline glm::vec3 bt_v3_to_glm(const btVector3 v) {
	return glm::vec3(v.getX(), v.getY(), v.getZ());
}

inline glm::quat bt_quat_to_glm(const btQuaternion q) {
	return conjugate(glm::quat(q.getW(), q.getX(), q.getY(), q.getZ()));
}

inline btVector3 glm_v3_to_bt(const glm::vec3 v) {
	return btVector3(v.x, v.y, v.z);
}

inline btQuaternion glm_quat_to_bt(const glm::quat q) {
	glm::quat c = conjugate(q);
	return btQuaternion(c.x, c.y, c.z, c.w);
}
