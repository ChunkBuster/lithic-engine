#pragma once
#include "ResourceManager.hpp"

namespace lithic_engine
{
	class Texture : public ResourceManager<Texture> {
	public:
		typedef std::map<std::string, Texture*> ImageMap;

		explicit Texture(std::string name);
		~Texture();

		unsigned int texture;

		int width;
		int height;
		int channels;
		GLuint format = 0;

	protected:
		//void parse_image(unsigned char *imageData);
		std::string dir() override;
		bool loadResource(std::string name, bool mt) override;

	private:
		bool initialized = false;
	};
}
