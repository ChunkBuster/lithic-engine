#pragma once
#include "lithic_engine/fwd.h"
#include "lithic_engine/resources/flatbuffers/MeshSchema_generated.h"
#include "lithic_engine/Defines.hpp"
namespace lithic_engine
{
	namespace import_utils
	{
		class MeshBufImporter
		{
			public:
				MeshBufImporter();
				~MeshBufImporter();
				static bool Import_MeshBuf(Mesh &manager, long timestamp = 0);
				static bool Save_MeshBuf(Mesh &manager, long timestamp = 0);
		};
	}
}

