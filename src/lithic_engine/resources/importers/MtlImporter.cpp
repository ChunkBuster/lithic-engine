#include "stdafx.h"
#include "MtlImporter.hpp"

#include "lithic_engine/resources/utilities/Mesh.hpp"
#include "lithic_engine/resources/Texture.hpp"
#include "lithic_engine/resources/Material.hpp"
#include "lithic_engine/resources/MeshImportUtils.hpp"
#include "lithic_engine/debugging/Logger.hpp"

#include <boost/algorithm/string/case_conv.hpp>
#include <boost/algorithm/string/trim.hpp>

#define MATERIAL_LOAD_DEBUG 0
namespace lithic_engine
{
	namespace import_utils {

		typedef std::tuple<Material*, MapType> mapInfo;
		typedef std::tuple<Material*, MapType, std::string> texLoadInfo;

		bool mtl_importer::import_mtl(Mesh &mesh, std::ifstream &file){
			std::string line;
			int currentIndex = 0;
			
			std::vector<texLoadInfo> loadMap;

			std::string mat_name;
			std::string name;
			while (std::getline(file, line)) {
				Material *current_material = Material::get_resource(mat_name, false).get();

#if MATERIAL_LOAD_DEBUG
				std::cout << line << std::endl;
#endif
				std::string header;
				line = boost::trim_copy(line);
				std::istringstream lineStream(line);
				//Read in the header
				lineStream >> header;
				header = boost::to_lower_copy(header);
				if (header.empty()) continue;

				//Line is a vertex line
				if (header == "newmtl") {
					lineStream >> mat_name;
#if MATERIAL_LOAD_DEBUG
					std::cout << "Loaded material " << mat_name << std::endl;
#endif
				}
				else if (header == "illum") {
					lineStream >> current_material->illum;
				}
				//Channel defs
				else if (header == "ka") {
					glm::vec3 color;
					lineStream >> current_material->spec_color;
				}
				else if (header == "map_ka") {
					lineStream >> name;
					loadMap.emplace_back(current_material, spec_map, name);
				}
				else if (header == "map_kg") {
					lineStream >> name;
					loadMap.emplace_back(current_material, gloss_map, name);
				}
				else if (header == "map_kr" || header == "map_ns") {
					lineStream >> name;
					loadMap.emplace_back(current_material, roughness_map, name);
				}
				else if (header == "kd") {
					glm::vec3 color;
					lineStream >> current_material->diffus_color;
					current_material->diffus_color.a = 1;
				}
				else if (header == "map_kd") {
					lineStream >> name;
					loadMap.emplace_back(current_material, diffus_map, name);
				}
				else if (header == "map_kn" || header == "map_bump" || header == "bump") {
					lineStream >> name;
					loadMap.emplace_back(current_material, normal_map, name);
				}
				else if (header == "ks") {
					glm::vec3 color;
					lineStream >> current_material->spec_color;
				}
				else if (header == "map_ks") {
					lineStream >> name;
					loadMap.emplace_back(current_material, spec_map, name);
				}
				else if (header == "ke") {
					glm::vec3 color;
					lineStream >> current_material->emissive_color;
				}
				else if (header == "map_ke") {
					lineStream >> name;
					loadMap.emplace_back(current_material, emissive_map, name);
				}
				else if (header == "ni") {
					glm::vec3 color;
					lineStream >> current_material->anisotropy;
				}
				else if (header == "map_ni") {
					lineStream >> name;
					loadMap.emplace_back(current_material, anisotropy_map, name);
				}
				//Specular exponent
				else if (header == "ns") {
					lineStream >> current_material->spec_exponent;
				}
				//Mask map
				else if(header == "map_d") {
					lineStream >> name;
					loadMap.emplace_back(current_material, clip_map, name);
				}
				//opacity
				else if (header == "d") {
					float transp;
					lineStream >> transp;
					current_material->opacity = transp;
					if (approx(current_material->opacity, 0.25f))
						current_material->clip = true;
				}
				//inverted opacity
				else if (header == "tr") {
					float transp;
					lineStream >> transp;
					current_material->opacity = 1 - transp;
					if (approx(current_material->opacity, 0.25f))
						current_material->clip = true;
				}
				else if (header == "#") {
					//Comment, nothin to do
				}
				else {
					LOG(fmt::format("Unable to parse line \"{}\" in mtl file for mesh {}", line, mesh.GetName()), LogType::Error);
				}
			}
			using namespace boost::asio::detail;


			std::vector<mapInfo> keyVec;
			std::vector<std::string> valVec;
			for (auto &tup : loadMap) {
				keyVec.emplace_back(std::get<0>(tup), std::get<1>(tup));
				std::string str = std::get<2>(tup);
				//Regex out those (some how standard) slash gore issues
				str = regex_replace(str, std::regex(R"(\\\\)"), "/");
				str = regex_replace(str, std::regex(R"(\\)"), "/");
				valVec.push_back(str);
			}


			//thread_group group;
			for (int i = 0; i < keyVec.size(); i++) {
				auto mat = std::get<0> (keyVec[i]);
				mat->allMaps[std::get<1>(keyVec[i])] = Texture::get_resource(valVec[i]);
			}
			LOG(fmt::format("Loaded {} textures for materials used on mesh {}", keyVec.size(), mesh.GetName()), LogType::Comment);
			return true;
		}

		import_utils::mtl_importer::mtl_importer() {
		}


		import_utils::mtl_importer::~mtl_importer() {
		}
	}
}