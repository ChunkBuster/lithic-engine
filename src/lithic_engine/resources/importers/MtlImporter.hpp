#pragma once
#include "lithic_engine/resources/Material.hpp"

namespace lithic_engine
{
	namespace import_utils
	{
		class mtl_importer {
		public:
			mtl_importer();
			static bool import_mtl(Mesh &manager, std::ifstream &file);
			~mtl_importer();
		};
	}
}
