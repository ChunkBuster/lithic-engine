#include "stdafx.h"
#include "src/lithic_engine/Defines.hpp"
#include "ObjImporter.hpp"
#include "lithic_engine/resources/utilities/Mesh.hpp"
#include "lithic_engine/resources/MeshImportUtils.hpp"
#include "MtlImporter.hpp"
#include "lithic_engine/debugging/Logger.hpp"
#include <boost/algorithm/string.hpp>
#include <unordered_set>
#include <map>
#include "lithic_engine/resources/SplitMesh.hpp"
namespace lithic_engine
{
	namespace import_utils {
		bool obj_importer::import_obj(Mesh &mesh, std::ifstream &file) {
			idxVec vertexIndices, uvIndices, normalIndices;
			std::vector<glm::vec3> temp_vertices;
			std::vector<glm::vec2> temp_uvs;
			std::vector<glm::vec3> temp_normals;

			//Current material
			std::string mtllib;
			std::string material;

			std::map<std::string, IndexGroup> matMap;
			bool smooth = false;
			std::string group;
			std::string object;
			//Load and parse the file
			std::string line;

			int currentIndex = 0;
			while (std::getline(file, line)) {
				if (matMap.find(material) == matMap.end()) {
					const IndexGroup indexGroup;
					matMap[material] = indexGroup;
				}

				IndexGroup *idex = &matMap[material];

#ifdef MESH_LOAD_DEBUG
				std::cout << line << std::endl;
#endif

				std::string header;
				std::istringstream lineStream(line);
				//Read in the header
				lineStream >> header;
				if (header.empty()) continue;

				//Line is a vertex line
				if (header == "v") {
					glm::vec3 vertex;
					lineStream >> vertex;
					temp_vertices.push_back(vertex);
					//idex->verts->push_back(vertex);
#ifdef MESH_LOAD_DEBUG
					std::cout << '(' << vertex.x << ',' << vertex.y << ',' << vertex.z << ')' << std::endl;
#endif
				}
				//Line is a UV line
				else if (header == "vt") {
					glm::vec2 uv;
					lineStream >> uv;
					temp_uvs.push_back(uv);
					//idex->uvs->push_back(uv);
#ifdef MESH_LOAD_DEBUG
					std::cout << '(' << uv.x << ',' << uv.y << ')' << std::endl;
#endif
				}
				//Line is a normal line
				else if (header == "vn") {
					glm::vec3 normal;
					lineStream >> normal;
					temp_normals.push_back(normal);
					//idex->normals->push_back(normal);
#ifdef MESH_LOAD_DEBUG
					std::cout << '(' << normal.x << ',' << normal.y << ',' << normal.z << ')' << std::endl;
#endif
				}
				//Line is a material data line
				else if (header == "usemtl") {
					lineStream >> material;
				}
				//Line is a face line
				else if (header == "f") {
					std::string tripletString;
					while (lineStream >> tripletString) {
						if (tripletString.empty()) continue;

						std::istringstream tripletStream(tripletString);
						std::string token;
						std::vector<int> indexData;

						while (std::getline(tripletStream, token, '/')) {
							if (token.empty()) {
								indexData.push_back(-1);
								continue;
							}
							const int val = stoi(token);
							indexData.push_back(val);

						}
#ifdef MESH_LOAD_DEBUG
						std::cout << ' ';
#endif
						//Push values to vectors
						mesh.pushIndexValue(indexData[0], vertexIndices);
						mesh.pushIndexValue(indexData[1], uvIndices);
						mesh.pushIndexValue(indexData[2], normalIndices);

						//Currently, the index is simply the 
						mesh.pushIndexValue(indexData[0], idex->vertIndices);
						mesh.pushIndexValue(indexData[1], idex->uvIndices);
						mesh.pushIndexValue(indexData[2], idex->normalIndices);

						int curInitIdex = currentIndex;
						idex->initialIndices.push_back(currentIndex++);
						matMap[material] = *idex;

						for (auto i = 0; i < indexData.size(); i++) {
							if (indexData[i] > std::numeric_limits<idxVal>::max() || indexData[i] < 0) {
								LOG(fmt::format("Model contained invalid index {}!", indexData[i]), LogType::Error);
							}
						}
					}

#ifdef MESH_LOAD_DEBUG
					std::cout << vertexIndices[vertexIndices.size() - 3] << "/" << vertexIndices[vertexIndices.size() - 2] << "/" << vertexIndices[vertexIndices.size() - 1];
					std::cout << '\n';
#endif

				}
				else if (header == "g") {
					lineStream >> group;
					//Do something with this later
				}
				else if (header == "s") {
					std::string flag;
					lineStream >> flag;
					smooth = flag != "off";
					//Do something with this later
				}
				else if (header == "o") {
					std::string name;
					lineStream >> name;
					object = name;
					//Do something with this later
				}
				else if (header == "mtllib") {
					lineStream >> mtllib;
				}
				//Line is a comment line
				else if (header == "#") {
					//Comment, nothin to do
				}
				else {
					LOG(fmt::format("Unable to parse line \"{}\" in obj file for mesh {}", line, mesh.GetName()), LogType::Error);
				}
			}

			for (unsigned int vertexIndex : vertexIndices) {
				const //off-by-one offset due to obj indexing being weird
				glm::vec3 vertex = temp_vertices[vertexIndex - 1];
				mesh.vertices.push_back(vertex);
			}

			for (unsigned int uvIndex : uvIndices) {
				const glm::vec2 uv = temp_uvs[uvIndex - 1];
				mesh.uvs.push_back(uv);
			}

			for (unsigned int normalIndex : normalIndices) {
				const glm::vec3 norm = temp_normals[normalIndex - 1];
				mesh.normals.push_back(norm);
			}

			//Calculate tangents and bitangents
			for (unsigned i = 0; i < mesh.vertices.size(); i += 3) {
				// Shortcuts for vertices
				glm::vec3 & v0 = mesh.vertices[i + 0];
				glm::vec3 & v1 = mesh.vertices[i + 1];
				glm::vec3 & v2 = mesh.vertices[i + 2];

				// Shortcuts for UVs
				glm::vec2 & uv0 = mesh.uvs[i + 0];
				glm::vec2 & uv1 = mesh.uvs[i + 1];
				glm::vec2 & uv2 = mesh.uvs[i + 2];

				// Edges of the triangle : position delta
				const glm::vec3 deltaPos1 = v1 - v0;
				const glm::vec3 deltaPos2 = v2 - v0;

				// UV delta
				const glm::vec2 deltaUV1 = uv1 - uv0;
				const glm::vec2 deltaUV2 = uv2 - uv0;

				float r;
				float val = (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
				if (val != 0)
					r = 1.0f / val;
				else
					r = 1;

				const glm::vec3 tangent = (deltaPos1 * deltaUV2.y - deltaPos2 * deltaUV1.y)*r;
				const glm::vec3 bitangent = (deltaPos2 * deltaUV1.x - deltaPos1 * deltaUV2.x)*r;

				mesh.tangents.push_back(tangent);
				mesh.tangents.push_back(tangent);
				mesh.tangents.push_back(tangent);

				mesh.binormals.push_back(bitangent);
				mesh.binormals.push_back(bitangent);
				mesh.binormals.push_back(bitangent);

			}


			std::vector<MaterialAssocData> materialData;

			auto old_vertices = mesh.vertices;
			auto old_uvs = mesh.uvs;
			auto old_normals = mesh.normals;
			auto old_tangents = mesh.tangents;
			auto old_binormals = mesh.binormals;

			std::map<FatPackedVertex, idxVal> out_combo_index_assoc;

			//Take the raw data and combine data-identical vertices, storing the mappings in mesh.indices
			auto indexMap = indexVBO(mesh.vertices, mesh.uvs, mesh.normals, mesh.tangents, mesh.binormals, mesh.indices);
			//Re-index submeshes
			for (auto &matPair : matMap) {
				IndexGroup &idex = matMap[matPair.first];
				for (unsigned int i = 0; i < idex.initialIndices.size(); i++) {
					FatPackedVertex packed = {
						old_vertices[idex.initialIndices[i]],
						old_uvs[idex.initialIndices[i]],
						old_normals[idex.initialIndices[i]]
					};

					idxVal index;
					if (!getSimilarFatVert(packed, indexMap, index))
						LOG(fmt::format("Could not find pre-indexed vert on mesh {}", mesh.GetName()), LogType::Error);

					idex.finalIndices.push_back(index);
				}
			}

			for (auto &matPair : matMap) {
				IndexGroup &idex = matMap[matPair.first];
				if (idex.finalIndices.empty())
					continue;
				idex.neighbors = GenerateNeighborhood(mesh.vertices, idex.finalIndices);
			}


#if MESH_SPLITTING
			for (const auto &matPair : matMap) {
				if (matPair.second.finalIndices.empty())
					continue;
				IndexGroup &idex = matMap[matPair.first];
				VertNeighborMap neighbors;
				splitMesh(idex.finalIndices, idex.finalIndicesSplit, idex.neighbors, mesh.vertices);
			}
#else
			for (const auto &matPair : matMap) {
				if (matPair.second.finalIndices.empty())
					continue;
				IndexGroup &idex = matMap[matPair.first];
				idex.finalIndicesSplit.push_back(idex.finalIndices);
			}
#endif



			std::string location = mesh.dir() + mtllib;
			std::ifstream mtlfile;
			mtlfile.open(location, std::ifstream::in);
			//FILE * file = fopen(c_str(location, "r");
			if (!mtlfile) {
				LOG(fmt::format("Unable to open mtllib {} for obj file {}", location, mesh.GetName()), LogType::Error);
				return false;
			}

			if (mtl_importer::import_mtl(mesh, mtlfile)) {
				//Assign final index values to all submeshes
				for (const auto &matPair : matMap) {
					for (const auto &v : matPair.second.finalIndicesSplit) {
						if (v.empty())
							continue;
						glm::tvec3<double> center(0);
						float size = mesh.vertices.size();
						for (const auto &vert : v) {
							center += mesh.vertices[vert];
						}
						center /= static_cast<double>(v.size());
						auto box = new BoundingBox(center, false);
						for (const auto &vert : v) {
							box->Encapsulate(mesh.vertices[vert]);
						}
						mesh.subMeshes.emplace_back(v, Material::get_resource(matPair.first, false), box);
					}
				}
			}
			mtlfile.close();
			return true;
		}


		import_utils::obj_importer::~obj_importer() {
		}
	}
}