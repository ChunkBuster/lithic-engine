#include "stdafx.h"
#include "MeshBufImporter.hpp"
#include "lithic_engine/resources/flatbuffers/MeshSchema_generated.h"
#include "lithic_engine/resources/utilities/Mesh.hpp"
#include "lithic_engine/resources/utilities/FlatbufImportUtils.hpp"
#include "lithic_engine/Defines.hpp"
#include "lithic_engine/resources/utilities/MeshbufImportUtils.hpp"
#include "lithic_engine/resources/utilities/ResourceDirs.hpp"

namespace lithic_engine
{

	namespace import_utils
	{
		std::string MeshFilename(Mesh &manager) {
			return CacheDir() + manager.GetName() + FLATBUF_MESH_EXTENSION;
		}

		MeshBufImporter::MeshBufImporter()
		{

		}


		MeshBufImporter::~MeshBufImporter()
		{
		}

		bool MeshBufImporter::Save_MeshBuf(Mesh &manager, long timestamp) {
			flatbuffers::FlatBufferBuilder builder(1024);
			const auto superSub = buildSubmesh(builder, manager.indices); //Submesh containing all other submeshes
			const auto submeshes = buildSubmeshes(builder, manager.subMeshes);
			const auto verts = buildVec3Array(builder, manager.vertices);
			const auto normals = buildVec3Array(builder, manager.normals);
			const auto tangents = buildVec3Array(builder, manager.tangents);
			const auto binormals = buildVec3Array(builder, manager.binormals);
			const auto uvs = buildVec2Array(builder, manager.uvs);
			const auto meshInfo = CreateMeshInfo(builder, timestamp, superSub, submeshes, verts, normals, uvs, tangents, binormals);
			builder.Finish(meshInfo);
			uint8_t *buf = builder.GetBufferPointer();
			const int size = builder.GetSize();
			std::ofstream file(MeshFilename(manager), std::ios::out | std::ios::binary);
			file.write(reinterpret_cast<char*>(buf), size);
			return true;
		}

		bool MeshBufImporter::Import_MeshBuf(Mesh &manager, long timestamp) {
			std::ifstream ifs(MeshFilename(manager), std::ios::binary | std::ios::ate);
			if (!ifs)
				return false;
			std::ifstream::pos_type pos = ifs.tellg();
			const int length = pos;
			char *pChars = new char[length];
			ifs.seekg(0, std::ios::beg);
			ifs.read(pChars, length);
			uint8_t *buffer_pointer = reinterpret_cast<uint8_t*>(pChars);

			auto meshInfo = GetMeshInfo(buffer_pointer);
			if (meshInfo->creationDate() != timestamp)
				return false;

			manager.indices = (*readSubmesh(meshInfo->superSub())).indices;
			manager.subMeshes = *readSubmeshes(meshInfo->submeshes());

			manager.vertices = *readVec3Array(meshInfo->verts());
			manager.normals = *readVec3Array(meshInfo->normals());
			manager.uvs = *readVec2Array(meshInfo->uvs());
			manager.tangents = *readVec3Array(meshInfo->tangents());
			manager.binormals = *readVec3Array(meshInfo->binormals());
			//manager.subMeshes = readSubmeshes(meshInfo->submeshes());
			return true;
		}
	}
}
