#pragma once
class Mesh;
namespace lithic_engine
{
	namespace import_utils
	{
		class obj_importer {
		public:
			obj_importer();
			static bool import_obj(Mesh &manager, std::ifstream &file);
			~obj_importer();
		};
	}
}