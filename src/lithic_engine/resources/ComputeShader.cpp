﻿#include "stdafx.h"
#include "Texture.hpp"
#include "Material.hpp"
#include "lithic_engine/Defines.hpp"
#include "lithic_engine/resources/utilities/ShaderImportUtils.hpp"
#include "ComputeShader.hpp"

namespace lithic_engine
{
	ComputeShader::ComputeShader(std::string name) :
		ResourceManager<ComputeShader>(name),
		shaderProgram(glCreateProgram()), globalBlock(0){
	}

	bool ComputeShader::loadResource(std::string name, bool mt) {
		const std::string computeLocation = dir() + name + COMPUTE_EXTENSION;
		GLuint computeShader(0);
		return 
			CompileShader(computeShader, computeLocation, Name, GL_COMPUTE_SHADER) &&
			LinkShader(shaderProgram, Name, computeShader);
	}

	std::string ComputeShader::dir() {
		return ResourceManager<ComputeShader>::dir() + COMPUTE_SHADER_LOCATION;
	}

	void ComputeShader::RecompileShaders() {
		for (auto &pair : ResourceMap) {
			ComputeShader *manager = ResourceMap[pair.first].get();
			manager->uniforms.clear();
			manager->loadResource(pair.first, false);
		}
	}

	ComputeShader::~ComputeShader() {
	}

	GLuint ComputeShader::GetUniform(std::string uniform_name) {
		GLuint uniformLocation = uniforms[uniform_name];
		if (!uniformLocation) {
			uniformLocation = glGetUniformLocation(shaderProgram, uniform_name.c_str());
			if (uniformLocation == GL_INVALID_VALUE)
				LOG(fmt::format("Attempted to get uniform \"{}\" from compute shader {} , but it was not found in the program", uniform_name,
					Name), LogType::Warning);
			uniforms[uniform_name] = uniformLocation;
		}
		return uniformLocation;
	}
}
