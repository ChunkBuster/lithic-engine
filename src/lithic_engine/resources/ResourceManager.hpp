#pragma once
#include <map>
#include <regex>
#include "lithic_engine/debugging/Logger.hpp"

namespace lithic_engine
{
	/**
	 * \brief Resources are managed through the Manager. If they are accessed and they do not yet exist, they are
	 * added to a lookup table of their specific type for later access.
	 * \tparam ResourceType Resource manager type for the derived class
	 */
	template <class ResourceType>
	class ResourceManager {
	public:
		explicit ResourceManager(std::string name);
		virtual ~ResourceManager();

		static std::shared_ptr<ResourceType> get_resource(std::string name, bool mt = false);
		static void assign_resource(std::string name, std::shared_ptr<ResourceType>);
		typedef std::map<std::string, std::shared_ptr<ResourceType>> resource_map;
		static resource_map ResourceMap;

		virtual std::string dir() = 0;

		std::string GetName() const {
			return Name;
		}
	protected:
		std::string Name;
		virtual bool loadResource(std::string name, bool mt = false) = 0;
		bool loaded = false;

	};


	template <class ResourceType>
	typename ResourceManager<ResourceType>::resource_map ResourceManager<ResourceType>::ResourceMap = {};


	template <class ResourceType>
	std::string ResourceManager<ResourceType>::dir() {
		const auto p = boost::filesystem::current_path();
		return std::string("resources/");
	}

	/**
	 * \brief name On instantiation, Resources add themselves to a lookup table
	 * \param name name of the resource
	 */
	template <class ResourceType>
	ResourceManager<ResourceType>::ResourceManager(std::string name) {
		Name = name;
	}

	template <class ResourceType>
	ResourceManager<ResourceType>::~ResourceManager() {

	}

	template <class ResourceType>
	/**
	* \brief
	* \param name Name of the resource to load
	* \return the loaded resource
	*/
	std::shared_ptr<ResourceType> ResourceManager<ResourceType>::get_resource(std::string name, bool mt) {
		std::shared_ptr<ResourceType> resource = ResourceMap[name];
		if (!resource) {
			auto res = std::make_shared<ResourceType>(name);
			auto genResource = std::static_pointer_cast<ResourceManager<ResourceType>>(res);
			if (!genResource->loadResource(name, mt))
			{
				LOG(fmt::format("Failed to load resource {}", name), LogType::Error);
			}
			else
			{
				res->loaded = true;
			}

			ResourceMap[name] = res;
			return res;
		}
		return std::static_pointer_cast<ResourceType>(resource);
	}

	template <class ResourceType>
	void ResourceManager<ResourceType>::assign_resource(std::string name, std::shared_ptr<ResourceType> resource) {
		ResourceMap[name] = resource;
		resource->Name = name;
	}
}