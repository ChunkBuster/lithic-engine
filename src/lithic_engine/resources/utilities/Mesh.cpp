#include "stdafx.h"
#include "src/lithic_engine/Defines.hpp"
#include "lithic_engine/resources/SurfaceShader.hpp"
#include "lithic_engine/resources/Texture.hpp"
#include "lithic_engine/resources/importers/ObjImporter.hpp"
#include "lithic_engine/game_logic/components/Renderer.hpp"
#include "lithic_engine/debugging/Logger.hpp"
#include "lithic_engine/resources/importers/MeshBufImporter.hpp"
#include "Mesh.hpp"
#include <boost/filesystem/operations.hpp>
//#include <iso646.h>

#define SHADOWMAP_SIZE 1024
namespace lithic_engine
{

	//#define MESH_LOAD_DEBUG
	Mesh::Mesh(std::string name) :
		ResourceManager(name), mesh(0) {

		}

	Mesh::~Mesh() {
		for (auto &m : subMeshes) {

		}
	}

	void Mesh::bindBuffers() {
		//Bind all vertex attribs to one object
		glGenVertexArrays(1, &_bindInfo.VAO);
		//Generate the five buffers of vertex attributes that are stored on rendered models

		BindVertexInfo(_bindInfo, vertices, uvs, normals, tangents, binormals, Name);

		//General ebo
		glGenBuffers(1, &_bindInfo.EBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _bindInfo.EBO);
		if (!indices.empty())
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(idxVal), &indices[0], GL_STATIC_DRAW);
		else
			LOG(fmt::format("No indices found for mesh {}", Name), LogType::Error);

		std::vector<idxVal> allIndices;
		for(const auto &s : subMeshes) {
			allIndices.insert(allIndices.end(), s.indices.begin(), s.indices.end());
		}

		//Submesh Composite ebo
		glGenBuffers(1, &_bindInfo.SubMeshSuperEBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _bindInfo.SubMeshSuperEBO);
		if (!indices.empty())
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, allIndices.size() * sizeof(idxVal), &allIndices[0], GL_STATIC_DRAW);
		else
			LOG(fmt::format("No indices found for mesh {}", Name), LogType::Error);

		//submesh ebos
		for (auto &sub : subMeshes) {
			glGenBuffers(1, &sub.EBO);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, sub.EBO);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, sub.indices.size() * sizeof(idxVal), &sub.indices[0], GL_STATIC_DRAW);
		}

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}

	void Mesh::pushIndexValue(int val, idxVec &vector) {
		if (val != -1)
			vector.push_back(val);
	}

	std::string Mesh::dir() {
		return ResourceManager<Mesh>::dir() + "meshes/";
	}

	bool Mesh::loadResource(std::string name, bool mt) {

		const boost::filesystem::path objLocation(dir() + name + OBJ_MESH_EXTENSION);
		const std::time_t time = last_write_time(objLocation);

		const long timestamp = static_cast<long>(time);
		const std::string meshbufLocation = RESOURCE_CACHE_DIR + name + FLATBUF_MESH_EXTENSION;
		std::ifstream objFile;

		objFile.open(objLocation.c_str(), std::ifstream::in);

		if (!objFile) {
			LOG(fmt::format("Unable to open {}", objLocation.string()), LogType::Error);
			return false;
		}

#if RESOURCE_CACHING
		if (!import_utils::MeshBufImporter::Import_MeshBuf(*this, timestamp)) {
			LOG(fmt::format("Could not get cached mesh for {}! Loading obj and generating cache...", objLocation.string()), LogType::Warning);
#else
			LOG(fmt::format("Caching disabled! Loading mesh...", objLocation.string()), LogType::Special);
#endif
			if (!import_utils::obj_importer::import_obj(*this, objFile)) {
				return false;
			}
#if RESOURCE_CACHING
			else {
				import_utils::MeshBufImporter::Save_MeshBuf(*this, timestamp);
				LOG(fmt::format("Generated cache for mesh {} ({})", objLocation.string(), meshbufLocation), LogType::Special);
			}
		}
		else {
			LOG(fmt::format("Loaded cached version of mesh {} from {}", objLocation.string(), meshbufLocation), LogType::Comment);
		}
#endif
		objFile.close();

		bindBuffers();
#if VERBOSE_RESOURCES
		LOG(fmt::format("Loaded mesh \"{}\"", name), LogType::Message);
		LOG(fmt::format("Contained {} submeshes: ", subMeshes.size()), LogType::Comment);
		int submeshCounter = 0;
		for (const auto & sub : subMeshes) {
			LOG(fmt::format("Submesh {} submeshes: ({} verts) using material {}",
						submeshCounter++, sub.indices.size(), sub.mat->name), LogType::Comment);
		}
#else
		LOG(fmt::format("Loaded mesh \"{}\" {} verts, {} submeshes."
					, name, vertices.size(), subMeshes.size()), LogType::Comment);
#endif
		return true;
	}

	MeshBindInfo & Mesh::GetBindingInfo() {
		return _bindInfo;
	}
}
