#pragma once
#include "lithic_engine/resources/flatbuffers/MeshSchema_generated.h"
#include <flatbuffers/flatbuffers.h>
namespace lithic_engine
{
	namespace import_utils
	{
		using namespace flatbuf::mesh;
		inline auto readVec2(const Vec2 *vec) {
			return glm::vec2(vec->x(), vec->y());
		}

		inline auto readVec3(const Vec3 *vec) {
			return glm::vec3(vec->x(), vec->y(), vec->z());
		}

		inline auto readVec4(const Vec4 *vec) {
			return glm::vec4(vec->x(), vec->y(), vec->z(), vec->w());
		}

		inline auto readVec3Array(const flatbuffers::Vector<const Vec3*>
				*vectors) {
			auto vec3Vec = new std::vector<glm::vec3>();
			for (const auto v : *vectors) {
				const glm::vec3 point(readVec3(v));
				vec3Vec->push_back(point);
			}
			return vec3Vec;
		}

		inline auto readVec2Array(const flatbuffers::Vector<const Vec2*>
				*vectors) {
			auto vec2Vec = new std::vector<glm::vec2>();
			for (const auto v : *vectors) {
				const glm::vec2 point(readVec2(v));
				vec2Vec->push_back(point);
			}
			return vec2Vec;
		}

		inline auto readFloatArray(const flatbuffers::Vector<float>
				*floats) {
			std::vector<float> outVec;
			auto length = floats->Length();
			for (const auto v : *floats) {
				const float val(v);
				outVec.push_back(v);
			}
			return outVec;
		}


		inline auto buildVec2(const glm::vec2 vec) {
			return Vec2(vec.x, vec.y);
		}

		inline auto buildVec3(const glm::vec3 vec) {
			return Vec3(vec.x, vec.y, vec.z);
		}

		inline auto buildVec4(const glm::vec4 vec) {
			return Vec4(vec.x, vec.y, vec.z, vec.w);
		}

		inline auto buildFloatArray(flatbuffers::FlatBufferBuilder &builder, std::vector<float> &floats) {
			std::vector<float> floatVec;
			for (const auto f : floats) {
				floatVec.push_back(f);
			}
			return builder.CreateVector(floatVec);
		}

		inline auto buildVec2Array(flatbuffers::FlatBufferBuilder &builder, std::vector<glm::vec2> &vectors) {
			std::vector<Vec2> vec2Vec;
			for (const auto v : vectors) {
				const Vec2 point = { v.x, v.y };
				vec2Vec.push_back(point);
			}
			return builder.CreateVectorOfStructs(vec2Vec);
		}


		inline auto buildVec3Array(flatbuffers::FlatBufferBuilder &builder, std::vector<glm::vec3> &vectors) {
			std::vector<Vec3> vec3Vec;
			for (const auto v : vectors) {
				const Vec3 point = { v.x, v.y, v.z };
				vec3Vec.push_back(point);
			}
			return builder.CreateVectorOfStructs(vec3Vec);
		}


		inline auto readString(const flatbuffers::String *name) {
			return std::string(name->c_str());
		}

	}
}
