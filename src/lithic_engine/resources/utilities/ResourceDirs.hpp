#pragma once
#include "src/lithic_engine/Defines.hpp"

namespace lithic_engine
{
	namespace import_utils
	{
		inline std::string CacheDir() {
			boost::filesystem::path dir(RESOURCE_CACHE_DIR);
			boost::filesystem::create_directory(dir);
			return dir.string();
		}
	}
}
