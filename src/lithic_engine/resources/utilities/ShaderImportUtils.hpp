﻿#pragma once
namespace lithic_engine
{
#define ERROR_LOG_SIZE 1024
#define LINK_SYMBOL u8"◈"

	static std::map<GLenum, const char*> shaderSymbols{
		{ GL_VERTEX_SHADER, u8"◇" },
	{ GL_FRAGMENT_SHADER, u8"◆" },
	{ GL_COMPUTE_SHADER, u8"◆" }
	};

	static std::map<GLenum, const char*> shaderNames{
		{ GL_VERTEX_SHADER, "Vertex" },
	{ GL_FRAGMENT_SHADER, "Fragment"},
	{ GL_COMPUTE_SHADER, "Compute" },
	{ GL_COMPUTE_SHADER, "Geometry" }
	};

	inline std::string ShaderPreProcess(std::string &in) {
		return "";
	}

	inline GLint CompileShader(GLuint &out_shader, const std::string &location, const std::string &name, 
		GLenum type) {
		GLint success = GL_FALSE;
		//Buffer to log errors into
		GLchar compilerMessage[ERROR_LOG_SIZE];

		std::ifstream inFile;
		inFile.open(location);
		if (!inFile) {
			LOG(fmt::format("Unable to open file {} for {} shader {}", location, shaderNames[type], name, std::string(compilerMessage)), LogType::
				Error);
			return GL_FALSE;
		}

		std::stringstream bufferFrag;
		bufferFrag << inFile.rdbuf();
		inFile.clear();
		inFile.close();
		std::string shaderSource = bufferFrag.str();
		const GLchar *shaderSourceCStr = shaderSource.c_str();
		LOG(fmt::format("Loaded files for shader {}", name), LogType::Comment);

		out_shader = glCreateShader(type);
		glShaderSource(out_shader, 1, &shaderSourceCStr, nullptr);
		glCompileShader(out_shader);
		glGetShaderiv(out_shader, GL_COMPILE_STATUS, &success);
		if (success != GL_TRUE) {
			glGetShaderInfoLog(out_shader, ERROR_LOG_SIZE, nullptr, compilerMessage);
			LOG(fmt::format("{} shader compilation failed : \n{}", shaderNames[type], std::
				string(compilerMessage)), LogType::Error);
			return GL_FALSE;
		}
		LOG(fmt::format("Compiled {} shader for {}", shaderNames[type], name), LogType::Special, shaderSymbols[type]);
		return success;
	}

	template<typename... Args> GLint LinkShader(GLuint &out_shader, const std::string &name, Args... shaders){
		GLchar compilerMessage[ERROR_LOG_SIZE];

		GLint statusCode = GL_FALSE;
		int infoLogLength;
		//Attach the shaders to the program

		std::vector<GLuint> vec = { shaders... };
		for (unsigned i = 0; i < vec.size(); i++) {
			glAttachShader(out_shader, vec[i]);
		}

		//Link the shader(s)
		glLinkProgram(out_shader);

		//Ensure that they actually linked
		glGetProgramiv(out_shader, GL_LINK_STATUS, &statusCode);
		if (statusCode != GL_TRUE) {
			glGetProgramInfoLog(out_shader, ERROR_LOG_SIZE, nullptr, compilerMessage);
			LOG(fmt::format("Linking shader program failed : \n{}", name, compilerMessage), LogType::Error);
			return GL_FALSE;
		}

		for (unsigned i = 0; i < vec.size(); i++) {
			glDetachShader(out_shader, vec[i]);
			glDeleteShader(vec[i]);
		}
		LOG(fmt::format("Successfully linked shader {}", name), LogType::Special, LINK_SYMBOL);

		return statusCode;
	}


}
