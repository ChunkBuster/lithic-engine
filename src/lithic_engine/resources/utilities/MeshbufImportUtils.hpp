#pragma once
#include "../flatbuffers/MeshSchema_generated.h"
#include "../Material.hpp"
#include "../Texture.hpp"
#include "FlatbufImportUtils.hpp"
#include "Mesh.hpp"

namespace lithic_engine
{
	namespace import_utils
	{

		inline auto readTex(const flatbuffers::String *name) {
			return  Texture::get_resource(readString(name), false);
		}

		inline auto readMat(const flatbuf::mesh::Material *mat) {
			auto material = std::make_shared<lithic_engine::Material>
				(lithic_engine::Material(mat->name()->str()));
			Material::assign_resource(mat->name()->str(), material);
			if (mat->diffusMap())
				material->allMaps[diffus_map] = readTex(mat->diffusMap());
			if (mat->normalMap())
				material->allMaps[normal_map] = readTex(mat->normalMap());
			if (mat->specMap())
				material->allMaps[spec_map] = readTex(mat->specMap());
			if (mat->roughnessMap())
				material->allMaps[roughness_map] = readTex(mat->roughnessMap());
			if (mat->ambMap())
				material->allMaps[amb_map] = readTex(mat->ambMap());
			if (mat->bumpMap())
				material->allMaps[bump_map] = readTex(mat->bumpMap());
			if (mat->clipMap())
				material->allMaps[clip_map] = readTex(mat->clipMap());

			//Make these conversions functions:
			material->diffus_color = readVec4(mat->diffusColor());
			material->spec_color = readVec3(mat->specColor());
			material->amb_color = readVec4(mat->ambColor());

			return material;
		}

		inline auto readBounds(const flatbuf::mesh::BoundingBox *bounds) {
			//, bounds->extents()
			return lithic_engine::BoundingBox(readVec3(bounds->center()),
					readFloatArray(bounds->extents()),
					DEBUG_BOUNDS_RENDERING);
		}

		inline auto readSubmesh(const flatbuf::mesh::Submesh *submesh) {
			const auto indices = submesh->indices();
			const auto indices_length = submesh->indices()->size();
			std::vector<GLuint>idx_vec;
			for (auto i = 0; i < indices_length; i++) {
				idx_vec.push_back(indices->Get(i));
			}
			std::string matName;
			const auto mat = submesh->material();

			BoundingBox *bound;
			if(submesh->bounds()) {
				bound = new BoundingBox(readBounds(submesh->bounds()));
			}else {
				bound = new BoundingBox(glm::vec3(0));
			}

			const Mesh::SubMesh* sub;
			//Todo: Replace origin with actual origin loaded from buffer
			if ((void*)mat != nullptr)
				sub = new  Mesh::SubMesh(idx_vec, readMat(mat), bound);
			else
				sub = new  Mesh::SubMesh(idx_vec,
						Material::get_resource("None", false), bound);
			return sub;
		}

		inline auto readSubmeshes(const flatbuffers::Vector<flatbuffers::Offset<flatbuf::mesh::Submesh>> *submeshes) {
			const auto submeshes_length = submeshes->size();
			auto submesh_vec = new std::vector<Mesh::SubMesh>();
			for (auto i = 0; i < submeshes_length; i++) {
				submesh_vec->push_back(*readSubmesh(submeshes->Get(i)));
			}
			return submesh_vec;
		}

		inline auto buildBounds(flatbuffers::FlatBufferBuilder &builder, lithic_engine::BoundingBox *bound) {
			const auto center = buildVec3(bound->GetCenter());
			auto extents = bound->GetBounds();
			const auto extent = buildFloatArray(builder, extents);
			BoundingBoxBuilder boundBuilder(builder);
			boundBuilder.add_center(&center);
			boundBuilder.add_extents(extent);
			return boundBuilder.Finish();
		}

		inline auto buildMat(flatbuffers::FlatBufferBuilder &builder, Material *mat) {

			typedef void (MaterialBuilder::*matStringFunc)(flatbuffers::Offset<flatbuffers::String>);
			typedef flatbuffers::Offset<flatbuffers::String> flatStrOff;
			std::vector<flatStrOff> flatStrings;
			std::vector<matStringFunc> flatStringFuncs;
			const auto addMap = [&builder, &flatStrings, &flatStringFuncs](Texture *manager, matStringFunc addFunc) {
				if (manager != nullptr) {
					auto name = builder.CreateString(manager->GetName());
					flatStrings.push_back(name);
					flatStringFuncs.push_back(addFunc);
				}
			};

			addMap(mat->allMaps[diffus_map].get(), &MaterialBuilder::add_diffusMap);
			addMap(mat->allMaps[normal_map].get(), &MaterialBuilder::add_normalMap);
			addMap(mat->allMaps[spec_map].get(), &MaterialBuilder::add_specMap);
			addMap(mat->allMaps[roughness_map].get(), &MaterialBuilder::add_roughnessMap);
			addMap(mat->allMaps[amb_map].get(), &MaterialBuilder::add_ambMap);
			addMap(mat->allMaps[bump_map].get(), &MaterialBuilder::add_bumpMap);
			addMap(mat->allMaps[clip_map].get(), &MaterialBuilder::add_clipMap);

			const auto name = builder.CreateString(mat->GetName());
			auto matBuilder = new MaterialBuilder(builder);
			matBuilder->add_name(name);
			const auto addColor3 = [&matBuilder](glm::vec3 Color, void(MaterialBuilder::*addFunc)(const Vec3 *)) {
				const Vec3 col = buildVec3(Color);
				(matBuilder->*addFunc)(&col);
			};

			const auto addColor4 = [&matBuilder](glm::vec4 Color, void(MaterialBuilder::*addFunc)(const Vec4 *)) {
				const Vec4 col = { Color.x, Color.y, Color.z, Color.w };
				(matBuilder->*addFunc)(&col);
			};

			addColor3(mat->spec_color, &MaterialBuilder::add_specColor);
			addColor4(mat->amb_color, &MaterialBuilder::add_ambColor);
			addColor4(mat->diffus_color * glm::vec4(1, 1, 1, mat->opacity), &MaterialBuilder::add_diffusColor);

			for (int i = 0; i < flatStrings.size(); i++) {
				(matBuilder->*flatStringFuncs[i])(flatStrings[i]);
			}
			return matBuilder->Finish();
		}

		inline auto buildSubmesh(flatbuffers::FlatBufferBuilder &builder, Mesh::SubMesh &mesh) {
			const auto int_vec = builder.CreateVector(&mesh.indices[0], mesh.indices.size());
			const auto mat = buildMat(builder, mesh.mat.get());
			const auto bound = buildBounds(builder, mesh.bounds);
			return CreateSubmesh(builder, int_vec, mat, bound);
		}

		inline auto buildSubmesh(flatbuffers::FlatBufferBuilder &builder, std::vector<unsigned int> &indices) {
			const auto int_vec = builder.CreateVector(&indices[0], indices.size());
			return CreateSubmesh(builder, int_vec);
		}

		inline auto buildSubmeshes(flatbuffers::FlatBufferBuilder &builder, std::vector<Mesh::SubMesh> &subMeshes) {
			std::vector<flatbuffers::Offset<Submesh>> submesh_vector;
			for (auto& mesh : subMeshes) {
				submesh_vector.push_back(buildSubmesh(builder, mesh));
			}
			return builder.CreateVector(submesh_vector);
		}
	}
}
