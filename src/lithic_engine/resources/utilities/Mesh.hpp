#pragma once
#include "src/lithic_engine/Defines.hpp"
#include "lithic_engine/resources/ResourceManager.hpp"
#include "lithic_engine/resources/Material.hpp"
#include "lithic_engine/utils/BoundingBox.hpp"
#include "lithic_engine/resources/MeshBindInfo.hpp"
#include "lithic_engine/rendering/BindVertexInfo.hpp"

#include <unordered_set>

namespace lithic_engine
{
	class Mesh : public ResourceManager<Mesh> {
		public:
			/**
			 * \brief Contains all of the information necessary to render this given submesh- Material data, bounds data for frustum clipping, and
			 * indices to be used while drawing from the parent mesh.
			 */
			struct SubMesh {
				idxVec indices;
				int numUniqueVerts = 0;
				GLuint EBO = 0;
				std::shared_ptr<Material> mat = nullptr;
				bool smooth = false;
				BoundingBox *bounds = nullptr;

				void calcUniqueVerts() {
					std::unordered_set<idxVal> indexSet;
					for (auto i : indices)
						indexSet.insert(i);
					numUniqueVerts = indexSet.size();
				}

				SubMesh(const SubMesh & toCopy) : smooth(false), bounds(toCopy.bounds) {
					indices = toCopy.indices;
					EBO = toCopy.EBO;
					mat = toCopy.mat;
					calcUniqueVerts();
				}

				SubMesh(idxVec ind, std::shared_ptr<Material> m, BoundingBox *bounds) : bounds(bounds) {
					indices = ind;
					mat = m;
					calcUniqueVerts();
				}

				SubMesh(idxVec ind, std::shared_ptr<Material> m, glm::vec3 center) : bounds(new BoundingBox(center, false)) {
					indices = ind;
					mat = m;
					calcUniqueVerts();
				}

				SubMesh& operator=(const SubMesh&) = default;

				/*
				   sub_mesh& operator=(const sub_mesh& other)
				   {
				 *this = sub_mesh(other);
				 return *this;
				 }*/

			};

			explicit Mesh(std::string name);
			~Mesh();

			MeshBindInfo &GetBindingInfo();
			static void pushIndexValue(int val, idxVec &vector);

			//Vectors usd to store backing mesh data
			std::vector<glm::vec3> vertices;
			std::vector<glm::vec2> uvs;
			std::vector<glm::vec3> normals;
			std::vector<glm::vec3> binormals;
			std::vector<glm::vec3> tangents;

			idxVec indices;
			//Binding point for submesh indices
			std::vector<SubMesh> subMeshes;

			unsigned int mesh;

			std::string dir() override;
		protected:
			bool loadResource(std::string name, bool mt) override;

		private:
			/**
			 * \brief Bind all of the buffers for this mesh
			 */
			void bindBuffers();
			MeshBindInfo _bindInfo;
	};
}
