#pragma once
#include "MeshImportUtils.hpp"
#include <unordered_set>
namespace lithic_engine
{
	namespace import_utils
	{
#define VERTS_IN_TRI 3
		
		/**
		 * \brief Generates a map containing neighbor information - called outside of this block and passed in
		 * \param verts The verts to generate the neighborhood for
		 * \param indices The indices that define the verts
		 * \return A map containing all neighborhood information
		 */
		inline VertNeighborMap GenerateNeighborhood(const std::vector<glm::vec3> &verts, const idxVec &indices) {
			VertNeighborMap map;
			for (auto i = 0; i < indices.size(); i += VERTS_IN_TRI) {
				for (auto x = 0; x < VERTS_IN_TRI; x++) {
					for (auto y = 0; y < VERTS_IN_TRI; y++) {
						if (x != y)
						{
							const auto val1 = indices[i + x];
							const auto val2 = indices[i + y];
							const VertKey pair1(verts[val1]);
							const VertKey pair2(verts[val2]);
							map[pair1].insert(pair2);;
						}
					}
				}
			}
			return map;
		}

		/**
		 * \brief Insert a triangle into the given idxVec
		 * \param vec vector to add to
		 * \param iter the location of the triangle
		 */
		inline void InsertTriangle(idxVec &vec, const idxVec::iterator &iter) {
			for (int i = 0; i < VERTS_IN_TRI; i++) {
				vec.push_back(*(iter + i));
			}
		}

		/**
		* \brief Insert a triangle into the unordered set of VertKeys
		* \param vec vector to add to
		* \param iter the location of the triangle
		* \param verts The vector containing vertex position information
		*/
		inline void InsertTriangle(std::unordered_set<VertKey> &set, const idxVec::iterator &iter, const std::vector<glm::vec3> &verts) {
			for (int i = 0; i < VERTS_IN_TRI; i++) {
				set.insert(verts[*(iter + i)]);
			}
		}

		/**
		 * \brief Spiders out from a vertex, recursively locating all connected verts
		 * \param vert The vertex to spider from
		 * \param neighbors The VertNeighborMap containing the neighborhood information
		 * \param outConnected The output collection containing all connected vertices
		 */
		inline void GetConnected(VertKey vert, VertNeighborMap &neighbors,
			std::unordered_set<VertKey> &outConnected) {
			std::unordered_set<VertKey> checked;
			std::unordered_set<VertKey> toCheck = { vert };
			while (!toCheck.empty()) {
				const auto iter = toCheck.begin();
				const VertKey index = *iter;
				checked.insert(index);
				outConnected.insert(index);

				toCheck.erase(iter);
				for (const auto& n : neighbors[index]) {
					if (checked.find(n) == checked.end() && outConnected.find(n) == outConnected.end())
						toCheck.insert(n);

					outConnected.insert(n);
				}
			}
		}

		
		/**
		 * \brief Given all vertices that constitute a mesh, spits out a vector containing all disconnected submeshes
		 * \param indices The indices of the mesh to be split
		 * \param outIndicesSplit The indices that constitute all of the separated meshes
		 * \param neighbors A map containing neighorhood information, see GenerateNeighborhood()
		 * \param verts A Vector containing the vertex positions that constitute this mesh
		 */
		inline void SplitMesh(const idxVec &indices, std::vector<idxVec> &outIndicesSplit, VertNeighborMap &neighbors, const std::vector<glm::vec3> &verts) {
			std::unique_ptr<idxVec>currIndices(new idxVec(indices));
			std::unordered_set<VertKey> checked;
			while (!currIndices->empty()) {
				const auto start = currIndices->begin();
				//outVec contains the indices being added to the current submesh, croppedVec is the vector of indices that could not be reached
				idxVec outVec;
				idxVec *croppedVec = new idxVec;
				std::unordered_set<VertKey> connected, disconnected;

				VertKey lastFound(verts[*start]);
				GetConnected(lastFound, neighbors, connected);
				//Advance by the number of verts in a tri, as connection to one implies connection to the other two
				for (auto iter = start; iter != currIndices->end(); std::advance(iter, VERTS_IN_TRI)) {
					//Auto-add the initial vert and its connected members
					if (iter == start) {
						InsertTriangle(outVec, iter);
						InsertTriangle(connected, iter, verts);
						continue;
					}
					const VertKey current = verts[*iter];

					const auto findConnected = connected.find(current);
					const auto findDisconnected = disconnected.find(current);

					if (findConnected != connected.end()) {
						lastFound = current;
						InsertTriangle(outVec, iter);
					}
					else {
						InsertTriangle(*croppedVec, iter);
					}
				}
				outIndicesSplit.push_back(outVec);
				for (const auto& val : outVec)
					checked.insert(VertKey(verts[val]));
				currIndices.reset(croppedVec);
			}
		}
	}
}