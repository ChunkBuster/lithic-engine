#pragma once

class MeshBindInfo {
public:
	GLuint FramebufferName = 0;
	GLuint QuadProgramID = 0;
	GLuint DepthMatrixID = 0;
	GLuint DepthProgramID = 0;
	GLuint VAO = 0;
	GLuint EBO = 0;
	GLuint SubMeshSuperEBO = 0;
	GLuint buffers[5];

	MeshBindInfo() : buffers() {
		glGenBuffers(5, buffers);
	}
};
