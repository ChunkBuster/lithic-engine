﻿#include "stdafx.h"
#include "Cubemap.hpp"
// Include GLEW
#include <GL/glew.h>
namespace lithic_engine
{

	Cubemap::Cubemap(std::string name) :
		ResourceManager<Cubemap>(name), texture(0) {

		}

	std::string Cubemap::dir() {
		return ResourceManager<Cubemap>::dir() + "cubemaps/";
	}

	bool Cubemap::loadResource(std::string name, bool mt) {
		auto loadFace = [this, &name](GLenum faceid, std::string face) {
			std::string location = dir() + name + "/" + face + ".jpg";
			//Load the image data
			unsigned char* imageData = stbi_load(location.c_str(), &width, &height, &channels, 0);

			if (!imageData) {
				LOG(fmt::format("Failed to load cubemap face {} for {}", face, name), LogType::Error);
				stbi_image_free(imageData);
				return false;
			}
			const GLenum format = channels == 3 ? GL_RGB : GL_RGBA;
			glTexImage2D(faceid, 0, GL_RGB, width, height, 0, format, GL_UNSIGNED_BYTE, imageData);
			stbi_image_free(imageData);
			return true;
		};

#if VERBOSE_RESOURCES
		LOG(fmt::format("Loaded image {} ({}px by {}px, {}channels)", name, width, height, channels), LogType::Comment, u8"🎨");
#endif
		glActiveTexture(GL_TEXTURE0);
		glGenTextures(1, &texture);
		//Binds the texture so that subsequent calls will modify this texture
		glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);

		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_BASE_LEVEL, 0);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAX_LEVEL, 10);

		if (!(
					loadFace(GL_TEXTURE_CUBE_MAP_POSITIVE_X, "posx") &&
					loadFace(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, "negx") &&
					loadFace(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, "posy") &&
					loadFace(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, "negy") &&
					loadFace(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, "posz") &&
					loadFace(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, "negz")
			 ))
		{
			LOG(fmt::format("Failed to load cubemap {}", name), LogType::Error);
			return false;
		}

		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
		glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

		//glHint(GL_GENERATE_MIPMAP_HINT, GL_NICEST);
		glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
		initialized = true;
		return true;
	}

	Cubemap::~Cubemap() {
	}
}
