#pragma once
#include "ResourceManager.hpp"
namespace lithic_engine
{
	class ComputeShader : public ResourceManager<ComputeShader> {
	public:
		explicit ComputeShader(std::string name);
		~ComputeShader();

		GLuint shaderProgram;

		GLuint globalBlock;
		/**
		* \brief Returns the uniform bound at a given string
		* \param name Name of the uniform to find
		* \return The uniform bound to that name
		*/
		GLuint GetUniform(std::string name);
		std::string dir() override;
		/**
		* \brief In-place recompile of shaders
		*/
		static void RecompileShaders();
	protected:
		bool loadResource(std::string name, bool mt) override;

	private:
		/**
		* \brief Contains all pre-indexed Uniforms for this shader
		*/
		std::map<std::string, GLuint> uniforms;
	};
}
