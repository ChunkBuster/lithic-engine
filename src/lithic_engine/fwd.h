#pragma once
#include <glm/fwd.hpp>
namespace lithic_engine
{
	class GameObject;
	class Component;
	class Texture;
	class SurfaceShader;
	class Cubemap;
	class Mesh;
	class Transform;
	class Weapon;
	class Renderer;
}

//Bullet
class btConvexHullShape;
class btShapeHull;
class btTriangleMesh;
class btBvhTriangleMeshShape;
class btCollisionShape;
class btDefaultMotionState;
class btRigidBody;
class btBroadphaseInterface;
class btCollisionDispatcher;
class btDefaultCollisionConfiguration;
class btSequentialImpulseConstraintSolver;
class btDiscreteDynamicsWorld;
