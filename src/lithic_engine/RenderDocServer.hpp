#pragma once
#if RENDERDOC_INTEGRATION
/*Skeletal RenderDoc integration. Added basic functionality (and some comments since other folks were having trouble with this)
In case this source file somehow makes it into somebody else's project (as I have shared it), the original author information is 
at the top, checking commit dates will verify that I am the author*/
#include "renderdoc_app.h"

#if _WIN32 || _WIN64
#define RENDERDOC_LIB "renderdoc.dll"
#else
#define RENDERDOC_LIB  "librenderdoc.so"
#endif

//Edit this to your preference
#define RDOC_DEFAULT_SETTINGS (\
rdoc_api->SetCaptureOptionU32(eRENDERDOC_Option_APIValidation, 1)&& /*Catch API errors*/\
rdoc_api->SetCaptureOptionU32(eRENDERDOC_Option_CaptureCallstacks, 1)&& /*Catch callstacks*/\
rdoc_api->SetCaptureOptionU32(eRENDERDOC_Option_DebugOutputMute, 0) /*Don't mute  debug output to the program itself*/\
)

/**
 * \brief Skeletal RenderDoc integration. Ensure that your RenderDoc handle location is in your PATH.
 * Documentation here: https://renderdoc.org/docs/in_application_api.html
 */
class RenderDocServer {
public:
	/**
	 * \brief Call Init as soon as possible in your application. 
	 */
	static void Init();
	/**
	 * \brief Optionally, add device handles if RenderDoc throws an exception on program exit.
	 * \param deviceHandle handle of the rendering device (Graphics API specific)
	 * \param windowHandle Handle of the window (Graphics API specific)
	 */
	static void AssignWindow(void *deviceHandle, void* windowHandle);
	/**
	 * \brief Call Close on program exit; failure to do so will cause an exception to be thrown.
	 * Note; If using GLFW, do not close your window at the end of your program; RenderDoc will 
	 * try to close it itself which will cause an exception to be thrown.
	 */	
	static void Close();
	/**
	 * \brief Capture a frame and immediately open it in the RenderDoc client
	 */
	static void Capture();
private:
	RenderDocServer();
	static bool Loaded();
	static std::unique_ptr<RenderDocServer> _instance;
	RENDERDOC_API_1_1_2* rdoc_api = nullptr;
	HMODULE rdoc_mod = nullptr;
};
#endif