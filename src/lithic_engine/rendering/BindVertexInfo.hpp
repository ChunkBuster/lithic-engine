#pragma once

namespace lithic_engine
{
	void BindVertexInfo(MeshBindInfo & _bindInfo, const std::vector<glm::vec3> &vertices,
		const std::vector<glm::vec2> &uvs,const std::vector<glm::vec3> &normals, const std::vector<glm::vec3> &tangents,
		const std::vector<glm::vec3> &binormals, const std::string &name);
}
