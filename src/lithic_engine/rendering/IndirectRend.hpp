#pragma once
#include "lithic_engine/resources/MeshBindInfo.hpp"
#include "lithic_engine/resources/importers/MtlImporter.hpp"
#include "lithic_engine/resources/utilities/Mesh.hpp"

namespace lithic_engine {
#if INDIRECT_RENDERING
#define NUM_DEBUG_COLORS 1000

	/**
	* \brief This class manages and controls the state of Indirect rendering. I designed it using a similar
	* state machine design principle to the one used by OpenGL.
	*/
	class IndirectRend
	{
	public:
		IndirectRend();
		~IndirectRend();
		static void Init();
		static idxVal GenerateDrawCommands(const Mesh &mesh);
		static void AddMesh(std::shared_ptr<Mesh> manager);
		static void Render();

	private:
		struct DrawElementsCommand{
			GLuint vertexCount;
			GLuint instanceCount;
			GLuint firstIndex;
			GLuint baseVertex;
			GLuint baseInstance;
		};

		struct DrawArraysIndirectCommand {
			GLuint count;
			GLuint instanceCount;
			GLuint first;
			GLuint baseInstance;
		} ;

		GLuint _colorArrayTex = 0;
		static std::unique_ptr<IndirectRend> _instance;
		MeshBindInfo BindInfoComplete;
		std::vector<unsigned int> _indices;
		std::vector<glm::vec3> _verts;
		std::vector<glm::vec2> _uvs;
		std::vector<glm::vec3> _normals;
		std::vector<glm::vec3> _tangents;
		std::vector<glm::vec3> _binormals;
		std::vector<DrawElementsCommand> _drawCommands;

		GLuint _indirectBuffer;
		GLuint _vertexOffset;
		GLuint _cmdIndex = 0;
		GLuint _commandHeadIndex = 0;

	};
#endif
}
