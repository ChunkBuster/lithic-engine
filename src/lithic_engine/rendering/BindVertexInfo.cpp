
#include "stdafx.h"
#include <GL/glew.h>
#include "lithic_engine/resources/MeshBindInfo.hpp"
#include "lithic_engine/debugging/Logger.hpp"

namespace lithic_engine
{
	inline void EnableAttribPointer(GLuint id, GLuint size) {
		glVertexAttribPointer(id, size, GL_FLOAT, GL_FALSE, 0, nullptr);
		glEnableVertexAttribArray(id);
	}

	void BindAttribArray(std::string name, GLuint& glBindPoint, GLuint pos, const 
		std::vector<glm::vec2>& data, GLenum drawType = GL_STATIC_DRAW) {
		glBindBuffer(GL_ARRAY_BUFFER, glBindPoint);
		if (!data.empty()) 
			glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(data[0]), &data[0], drawType);
		else 
			LOG(fmt::format("No {} found", name), LogType::Error);
		
		EnableAttribPointer(pos, 2);
	}

	void BindAttribArray(std::string name, GLuint& glBindPoint, GLuint pos, const
		std::vector<glm::vec3>& data, GLenum drawType = GL_STATIC_DRAW) {
		glBindBuffer(GL_ARRAY_BUFFER, glBindPoint);
		if (!data.empty()) 
			glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(data[0]), &data[0], drawType);
		else 
			LOG(fmt::format("No {} found", name), LogType::Error);
		
		EnableAttribPointer(pos, 3);
	}

	void BindVertexInfo(MeshBindInfo & _bindInfo, const std::vector<glm::vec3> &vertices,
		const std::vector<glm::vec2>& uvs, const std::vector<glm::vec3>& normals, const std::vector<glm::vec3>& tangents,
		const std::vector<glm::vec3>& binormals, const std::string &name) {
		glBindVertexArray(_bindInfo.VAO);

		BindAttribArray("Vertices", _bindInfo.buffers[0], 0, vertices);
		BindAttribArray("UVs", _bindInfo.buffers[1], 1, uvs);
		BindAttribArray("Normals", _bindInfo.buffers[2], 2, normals);
		BindAttribArray("Tangents", _bindInfo.buffers[3], 3, tangents);
		BindAttribArray("Binormals", _bindInfo.buffers[4], 4, binormals);

		glBindVertexArray(0);
	}
}
