#pragma once
#include "lithic_engine/fwd.h"
#include "lithic_engine/game_logic/Component.hpp"

namespace lithic_engine
{
#define BOUNDS_SIZE 6
	/**
	 * \brief Describes an Axis Aligned Bounding Box 
	 *          maxY
          +--------------+
          |              |
          |   x          |
          |              |
      minX|              |maxX
          |              |
          |              |
          |              |
          +--------------+
                minY
	The Center is completely arbitrary and is often chosen as the center of
	the object that uses the bounding box. To get the actual center. use the 
	GetTrueCenter method.
	 */
	class BoundingBox
	{
	public:
		BoundingBox(const BoundingBox &other);
		BoundingBox(bool draw);
		BoundingBox(glm::vec3 center, bool draw = false);
		BoundingBox(glm::vec3 center, std::vector<float> bounds, bool draw = false);
		BoundingBox(glm::vec3 center, std::vector<glm::vec3> points, bool draw = false);
		~BoundingBox();

		BoundingBox& operator=(const BoundingBox&) = default;

		/**
		 * \brief Returns the center of the bounding box, determined on construction
		 * \return The center of this bounding box
		 */
		glm::vec3 GetCenter() const;

		/**
		 * \brief Returns the actual median point
		 * \return The actual coordinates of the true middle of the bounding box
		 */
		glm::vec3 GetTrueCenter() const;

		/**
		 * \brief Returns the half-extents of the bounds.
		 * \return Half-extents
		 */
		glm::vec3 GetExtents() const;

		/**
		 * \brief Returns the raw bounding values
		 * \return The raw maxX-minZ values.
		 */
		std::vector<float> GetBounds() const;
		/**
		 * \brief Checks if a point is within the bounds
		 * \param point Point to check
		 * \return true if point is within bounds, false otherwise
		 */
		bool ContainsPoint(glm::vec3 point);

		/**
		 * \brief Grow bounds to contain point
		 * \param point Point to encapsulate
		 */
		void Encapsulate(glm::vec3 point);

		void Update();
#if BOUNDS_RENDERING
		bool EnableRendering();
		bool DisableRendering();
#endif
	private:
		glm::vec3 center;
		float bounds[6];
		//Max values are ref-bound to the bounds array while retaining semantic usability
		float &maxX, &maxY, &maxZ, &minX, &minY, &minZ;
	private:
		Renderer * renderer = nullptr;
	};

}

