#include "stdafx.h"
#include "BoundingBox.hpp"
#include "lithic_engine/resources/utilities/Mesh.hpp"
#include "lithic_engine/game_logic/Component.hpp"
#include "lithic_engine/Game.hpp"
#include "lithic_engine/game_logic/Transform.hpp"

namespace lithic_engine
{
	BoundingBox::BoundingBox(glm::vec3 center, std::vector<glm::vec3> points, bool draw) :
		BoundingBox(center, draw) {
			for (const auto v : points) {
				Encapsulate(v);
			}
		}

	BoundingBox::BoundingBox(const BoundingBox &other) :
		BoundingBox(other.center, other.renderer != nullptr) {
			for (auto i = 0; i < BOUNDS_SIZE; i++)
				bounds[i] = other.bounds[i];

		}

	BoundingBox::BoundingBox(bool draw) : BoundingBox(glm::vec3(0), draw) { }

	BoundingBox::BoundingBox(glm::vec3 center, bool draw) : BoundingBox(center, std::vector<float>(BOUNDS_SIZE), draw) { }

	BoundingBox::BoundingBox(glm::vec3 center, std::vector<float> bound, bool draw) :
		center(center),
		bounds{ bound[0], bound[1],  bound[2],  bound[3],  bound[4],  bound[5] },
		maxX(bounds[0]), maxY(bounds[1]), maxZ(bounds[2]),
		minX(bounds[3]), minY(bounds[4]), minZ(bounds[5]){
#if BOUNDS_RENDERING
			if (draw) {
				EnableRendering();
			}
#endif
		}

	BoundingBox::~BoundingBox() = default;

	glm::vec3 BoundingBox::GetCenter() const {
		return center;
	}

	glm::vec3 BoundingBox::GetTrueCenter() const {
		return (center + glm::vec3(glm::vec3((bounds[0], bounds[1], bounds[2]))) + (center + glm::vec3((bounds[3], bounds[4], bounds[5]))))* 0.5f;
	}

	std::vector<float> BoundingBox::GetBounds() const {
		return std::vector<float>(bounds, bounds + BOUNDS_SIZE);
	}

	glm::vec3 BoundingBox::GetExtents() const {
		return glm::vec3(glm::vec3(bounds[0], bounds[1], bounds[2]) - glm::vec3(bounds[3], bounds[4], bounds[5])) * 0.5f;
	}

	bool BoundingBox::ContainsPoint(glm::vec3 point) {
		for (auto i = 0; i < BOUNDS_SIZE / 2; i++) {
			const float dist = point[i] - center[i];
			if (dist > 0 && dist > bounds[i])
				return false;
			else if (dist < 0 && dist < bounds[i + 3])
				return false;
		}
		return true;
	}

	void BoundingBox::Encapsulate(glm::vec3 point) {
		for (auto i = 0; i < BOUNDS_SIZE / 2; i++) {
			const float dist = point[i] - center[i];
			if (dist > 0 && dist > bounds[i])
				bounds[i] = dist;
			else if (dist < 0 && dist < bounds[i + BOUNDS_SIZE / 2])
				bounds[i + 3] = dist;
		}
	}

	void BoundingBox::Update() {
		const float height = (maxY - minY) / 2.0f;
		const float depth = (maxZ - minZ) / 2.0f;
		const float width = (maxX - minX) / 2.0f;

		if (renderer != nullptr) {
			renderer->transform->position = center;// +glm::vec3(maxX + minX, maxY + minY, maxZ + minZ);;
			renderer->transform->scale = glm::vec3(width, height, depth);
		}
	}

#if BOUNDS_RENDERING
	bool BoundingBox::EnableRendering() {
		if (renderer != nullptr) return false;
		renderer = new Renderer(Mesh::get_resource("Cube", false));
		renderer->drawMode = GL_LINE;
		Game::AddRenderer(renderer);
		return true;
	}


	bool BoundingBox::DisableRendering() {
		if (renderer != nullptr) return false;
		Game::RemoveMesh(renderer);
		delete renderer;
		return true;
	}
#endif
}
