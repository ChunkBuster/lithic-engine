﻿#pragma once
#include "src/lithic_engine/fwd.h"
#include "src/lithic_engine/Defines.hpp"

namespace user_input
{
	struct ExampleAppConsole;
}


namespace lithic_engine
{
	namespace debug
	{
		
#define LOG2(x, y) debug::Logger::log_imp(x, y, __LINE__, __func__, __FILE__)
#define LOG3(x, y, z) debug::Logger::log_imp(x, y, z, __LINE__, __func__, __FILE__)
#define LOG(...) CALL_OVERLOAD(LOG, __VA_ARGS__)

		class Logger {
		public:
			Logger();
			~Logger();
			static void log_imp(std::string message, LogType LogType, int line, const char *func, const char *file);
			static void log_imp(std::string message, LogType logType, std::string glyph, int line, const char *func,
				const char *file);

			static void set_log_level(int level);
			static void draw_console();

		private:
			static Logger *_Instance;
			user_input::ExampleAppConsole *_console;
			int _logLevel = 0;
			int _messages = 0;
			int _warnings = 0;
			int _errors = 0;
		};


		/**
		 * \brief Callback registered to GLFW internal errors; assigned in Game.cpp
		 * \param error Error thrown
		 * \param message Message associated with error
		 */
		void GlfwErrorCallback(int error, const char * message);

		void APIENTRY OglCallDebugCallback(GLenum source,
			GLenum type,
			GLuint id,
			GLenum severity,
			GLsizei length,
			const GLchar* message,
			const void* userParam);
	}
}