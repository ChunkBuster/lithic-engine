﻿#pragma once
namespace lithic_engine
{
	namespace debug {
		class DebuggingTest
		{
		public:
			static void ParticleTest();
			static void SpawnBallDemo(int size);
		};
	}
}
