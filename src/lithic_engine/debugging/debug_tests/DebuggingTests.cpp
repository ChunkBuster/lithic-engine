﻿#include "DebuggingTests.hpp"
#include "lithic_engine/game_logic/components/ParticleSystem.hpp"
#include "lithic_engine/game_logic/components/Renderer.hpp"
#include "lithic_engine/game_logic/GameObject.hpp"
#include "lithic_engine/resources/Material.hpp"
#include "lithic_engine/resources/Texture.hpp"
#include "lithic_engine/resources/utilities/Mesh.hpp"
#include "stdafx.h"

namespace lithic_engine
{
	namespace debug {
			void DebuggingTest::ParticleTest()
			{
				auto obj = GameObject::Instantiate("Test");

				auto particleSystem = obj->AddComponent<ParticleSystem>(10000, "snowflake", glm::vec3(0), glm::vec3(20), 0.5f, true);
				particleSystem->SetLife(7, 2);
				particleSystem->SetGravity(-0.1);
				particleSystem->SetScale(0.03, 0.01);
				particleSystem->SetVolume(30, 10, 15);
				particleSystem->SetCenter(-17, 5, -8);
				particleSystem->SetGravity(-0.2);
				particleSystem->SetTurbulence(0.04f);
				particleSystem->SetNoiseStrength(0.02f);
				particleSystem->UpdateUniforms();
			}


			void DebuggingTest::SpawnBallDemo(int size) {
				for (int i = 0; i < size; i++) {
					for (int u = 0; u < size; u++) {
						glm::vec3 middle = glm::vec3(8, 1, -((size - 2) * 1.3f) / 2);
						char n[3] = { char('0' + i), char('0' + u), '\0' };

						auto ballObj = GameObject::Instantiate(fmt::format("Ball {}", std::string(n)));
						auto rend = ballObj->AddComponent<Renderer>(Mesh::get_resource("Sphere"));



						Material *mat = new Material(std::string(n));
						mat->spec_color = glm::vec3((static_cast<float>(1) / static_cast<float>(size - 1)) * i);
						mat->roughness = (static_cast<float>(1) / static_cast<float>(size - 1)) * i;
						mat->allMaps[diffus_map] = Texture::get_resource("gray.png");
						rend->opaqueSubmeshes[0].mat.reset(mat);

						ballObj->transform->position = glm::vec3(0.0f, static_cast<float>(u) / 1.3f, static_cast<float>(i) / 1.3f) + middle;

						ballObj->transform->scale = glm::vec3(0.4);
					}
				}
			}
	}
}