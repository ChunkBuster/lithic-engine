﻿#include "stdafx.h"
#include "Logger.hpp"
#include "imgui_console.hpp"
namespace lithic_engine
{
	namespace debug
	{
		Logger *Logger::_Instance = new Logger();

		Logger::Logger() : _console(new user_input::ExampleAppConsole()) {

		}


		Logger::~Logger() {
		}

		void Logger::log_imp(std::string message, LogType logType, int line, const char *func, const char *file) {
			std::string prefix;
			switch (logType) {
			case(Comment):
				prefix = LOG_COMMENT_SYMBOL;
				break;
			case(Special):
			case(Message):
				prefix = LOG_MESSAGE_SYMBOL;
				break;
			case(Warning):
				prefix = LOG_WARNING_SYMBOL;
				break;
			default:
			case(Error):
				prefix = LOG_ERROR_SYMBOL;
				break;
			}
			log_imp(message, logType, prefix, line, func, file);
		}


		void Logger::log_imp(std::string message, LogType logType, std::string glyph, int line, const char *func,
			const char *file) {
			switch (logType) {
			case(Error):
				_Instance->_errors++;
#if STDOUT_LOGGING
				std::cerr << message << '\n';
#endif
				break;
			case(Warning):
				_Instance->_warnings++;
#if STDOUT_LOGGING
				std::cout << message << '\n';
#endif
				break;
			default:
			case(Message):
				_Instance->_messages++;
#if STDOUT_LOGGING
				std::cout << message << '\n';
#endif
				break;

			}
			std::string &usable_prefix = glyph;
			std::string suffix;
#if CODE_CONTEXT_ERRORS
			if (logType == Error)
				suffix = fmt::format(u8"│file \"{}\"│func \"{}\"│line {}", file, func, line);
#endif
			std::string formatted = fmt::format(u8"{}{}│{}{}", logType, usable_prefix, message, suffix);
			const char *message_cstr = formatted.c_str();
			_Instance->_console->AddLog(message_cstr);
		}

		void Logger::set_log_level(int level) {
			_Instance->_logLevel = level;
		}

		
		/**
		 * \brief Draws the console using imgui
		 */
		void Logger::draw_console() {
			std::stringstream title;
			title << u8"Console │" << _Instance->_messages << u8" Messages │" << _Instance->_warnings << u8" Warnings│" <<
				_Instance->_errors << u8" Errors";
			const std::string &tmp = title.str();
			const char *title_str(tmp.c_str());
			_Instance->_console->Draw(title_str);
		}

		void GlfwErrorCallback(int error, const char * message) {
			LOG(fmt::format("OpenGL error {} : {}", error, message), LogType::Error);
		}

		
		/** todo: Is this still necessary, or is it covered by the glfwErrorCallback? I can't think of an easy way to check
		 * \brief APIEntry for Internal OpenGL debug callbacks. This is all defined by OpenGL itself, don't change its name.
		 */
		void APIENTRY OglCallDebugCallback(GLenum source,
			GLenum type,
			GLuint id,
			GLenum severity,
			GLsizei length,
			const GLchar* message,
			const void* userParam) {

			LogType logType = LogType::Error;
			switch (severity) {
			case GL_DEBUG_SEVERITY_HIGH:
				logType = LogType::Error;
				break;
			case GL_DEBUG_SEVERITY_MEDIUM:
				logType = LogType::Warning;
				break;
			case GL_DEBUG_SEVERITY_LOW:
				logType = LogType::Message;
				break;
			case GL_DEBUG_SEVERITY_NOTIFICATION:
				logType = LogType::Comment;
				break;
			default:
				logType = LogType::Warning;
				break;
			}
			std::string sourceString;
			switch (source) {
			case GL_DEBUG_SOURCE_API:
				sourceString = "OpenGL API";
				break;
			case GL_DEBUG_SOURCE_SHADER_COMPILER:
				sourceString = "Shader Compiler";
				break;
			case GL_DEBUG_SOURCE_THIRD_PARTY:
				sourceString = "Third Party";
				break;
			case GL_DEBUG_SOURCE_APPLICATION:
				sourceString = "This Application";
				break;
			default:
			case GL_DEBUG_SOURCE_OTHER:
				sourceString = "Other";
				break;
			}
			LOG(fmt::format("{} source: {}", message, sourceString), logType, u8"❒");
		}
	}
}