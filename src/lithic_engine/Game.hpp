﻿#pragma once
#include "lithic_engine/game_logic/components/Renderer.hpp"
#include "lithic_engine/game_logic/components/MeshPhysicsObject.hpp"
#include <unordered_set>
using namespace lithic_engine;
	class Game {
	public:
		static void AddRenderer(Renderer *rend);
		static void RemoveRenderer(Renderer *rend);

		static void AddGameObject(GameObject* gameObject);
		static void RemoveGameObject(GameObject* gameObject);

		static void ChangeMap(std::string map);
		static void Render();
		static void Update();

		static std::unordered_set<Component*> GetComponents();

	private:
		static Game *Instance;
		std::unordered_set<Renderer*> Renderers;
		GameObject* Map = nullptr;
#if BULLET_PHYSICS
		std::unique_ptr<PhysicsObject> MapCollision = nullptr;
#endif
		std::unordered_set<GameObject*> GameObjects;
		std::unordered_set<Component*> components;
	};

