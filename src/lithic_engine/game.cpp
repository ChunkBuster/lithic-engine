﻿#include "stdafx.h"
#include "Game.hpp"
#include "utils/BoundingBox.hpp"
// Include GLEW for Pointer Management for GLFW

#include "game_logic/InputManager.hpp"
//Include user-defined header
#include "debugging/debug_tests/DebuggingTests.hpp"
#include "resources/utilities/Mesh.hpp"
#include "game_logic/components/Renderer.hpp"
#include "game_logic/components/Weapon.hpp"

#include "game_logic/components/physics/PhysObj.hpp"
#include "game_logic/PhysWorld.hpp"
#include "game_logic/components/MeshPhysicsObject.hpp"
#include "resources/Cubemap.hpp"
#include "resources/ComputeShader.hpp"
#include "resources/importers/MeshBufImporter.hpp"

#include "renderdoc_app.h"
#include "Defines.hpp"
#include "rendering/IndirectRend.hpp"
#include "resources/Texture.hpp"
#include "game_logic/components/ParticleSystem.hpp"
#include "game_logic/GameObject.hpp"
#include "debugging/Logger.hpp"
#if RENDERDOC_INTEGRATION
#include "RenderDocServer.hpp"
#endif
#if WALLPAPER_MODE
#include "winuser.h"
#endif

using namespace lithic_engine;
//#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")

int main() {
	//Init RenderDoc before anything else. RenderDoc is an invaluable graphics debugging tool
	//that I've been using to debug indirect rendering issues
#if RENDERDOC_INTEGRATION
	RenderDocServer::Init();
#endif

	srand(time(nullptr));
	setlocale(LC_ALL, "");
	glfwSetErrorCallback(debug::GlfwErrorCallback);

	if (!glfwInit()) {
		LOG("glfw could not init!", LogType::Error);
	}



    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);

	//glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_FALSE);

	//Get a pointer to a window object, created with the iecified params
	GLFWwindow *window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "Simple FPS", nullptr, nullptr);

	//If the window cannot be created, return a simple error and finish.
	if (window == nullptr) {
		std::cerr << "Failed to create GLFW window" << std::endl;
		//Terminate the glfw sessio
		glfwTerminate();
		//Return error code
		return -1;
	}

	//Set the active context to the current window
	glfwMakeContextCurrent(window);

	//vsync
	glfwSwapInterval(1);

	//std::vector<GLclampf> clearColor = {GLclampf(0), GLclampf(0), GLclampf(0), GLclampf(1)};
	// Initialize GLEW
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK) {
		LOG("Failed to initialize GLEW", LogType::Error);
		getchar();
		glfwTerminate();
		exit(-1);
	}

#if DEBUG
	glEnable(GL_DEBUG_OUTPUT);
	glDebugMessageCallback(debug::OglCallDebugCallback, nullptr);
#endif

	char* version;
	LOG(fmt::format("Initted with OpenGL version {}", glGetString(GL_VERSION)), LogType::Special);

#if INDIRECT_RENDERING
	IndirectRend::Init();
#endif

	InputManager::setCursorLock(window, false);
	InputManager::init_ui(window);

	std::vector<GLclampf> clearColor = { GLclampf(0), GLclampf(0.4), GLclampf(0.7), GLclampf(1) };
#if BULLET_PHYSICS
	//Init floor
	//btCollisionShape* groundShape = new btStaticPlaneShape(btVector3(0, 1, 0), 1);
	//btConvexHullShape* groundShape = new btConvexHullShape((const btScalar*)(&(v.xyzw[0])), glmesh->m_numvertices, sizeof(GLInstanceVertex));
#endif


	//Init skybox
	auto skybox = GameObject::Instantiate("Skybox");
	auto sRend = skybox->AddComponent<Renderer>(Mesh::get_resource("cube", false));
	sRend->SetDepthMode(false);
	sRend->SetShader(SurfaceShader::get_resource("skybox", false));
	sRend->CullingEnabled = false;
	
	//Set up texture binding points
	auto defaultShader = SurfaceShader::get_resource(DEFAULT_SHADER);
	glUseProgram(defaultShader->shaderProgram);
	glUniform1i(defaultShader->GetUniform("u_AlbedoMap"), diffus_map);
	glUniform1i(defaultShader->GetUniform("u_SpecularMap"), spec_map);
	glUniform1i(defaultShader->GetUniform("u_RoughnessMap"), roughness_map);
	glUniform1i(defaultShader->GetUniform("u_NormalMap"), normal_map);
	glUniform1i(defaultShader->GetUniform("u_ClipMap"), clip_map);
	glUniform1i(defaultShader->GetUniform("u_EnvironmentMap"), env_map);
	glUniform1i(defaultShader->GetUniform("u_PreintegratedFG"), preint_fg);
	//The preint fg never changes, so I bind it once here
	glActiveTexture(GL_TEXTURE0 + preint_fg);
	static GLuint preIntegratedFG = Texture::get_resource("pbr_texmaps/PreintegratedFG.bmp", false)->texture;
	glBindTexture(GL_TEXTURE_2D, preIntegratedFG);
	glUseProgram(0);


	//Load Map
	TIME_CALL(Game::ChangeMap("sponza_reduced_further"));

	//Run tests, if enabled
#if BALL_SHADER_TEST
	debug::DebuggingTest::SpawnBallDemo(5);
#endif

#if PARTICLE_TEST
	debug::DebuggingTest::ParticleTest();
#endif
	
	while (!glfwWindowShouldClose(window)) {
		globals::Time::StepTime();
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glDepthMask(GL_TRUE);
		glClearColor(clearColor[0], clearColor[1], clearColor[2], clearColor[3]);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		Game::Update();
		//Render the game
		Game::Render();

		//Polls events
		glfwPollEvents();

		//Draw imgui
		InputManager::draw_imgui();

		//Detect keypresses
		InputManager::process_input(window);

		//Updates the window
		glfwSwapBuffers(window);
	}

#if RENDERDOC_INTEGRATION
	RenderDocServer::Close();
#else
	glfwDestroyWindow(window);
	glfwTerminate();
#endif
	exit(0);
}

Game *Game::Instance = new Game();

void Game::AddRenderer(Renderer *rend) {
	Instance->Renderers.insert(rend);
}

void Game::RemoveRenderer(Renderer *rend) {
	const auto find = Instance->Renderers.find(rend);
	if(find != Instance->Renderers.end())
		Instance->Renderers.erase(find);
}

void Game::AddGameObject(GameObject *gameObject) {
	Instance->GameObjects.insert(gameObject);
}

void Game::RemoveGameObject(GameObject *gameObject) {
	const auto find = Instance->GameObjects.find(gameObject);
	if (find != Instance->GameObjects.end())
		Instance->GameObjects.erase(find);
}

void Game::ChangeMap(std::string map_name) {
	LOG(fmt::format("Changing map to {}...", map_name), LogType::Message);
#if MAP_BOUNDS_RENDERING
	if(Instance->Map)
		for (auto &m : Instance->Map->opaque_sub_meshes) {
			m.bounds->DisableRendering();
		}
#endif
	GameObject::Destroy(Instance->Map);
	auto test = InputManager::playerObject.get();
	auto newMap = GameObject::Instantiate("Map");
	auto rMap = newMap->AddComponent<Renderer>(Mesh::get_resource(map_name, false));
#if BULLET_PHYSICS
	auto pMap = newMap->AddComponent<MeshPhysicsObject>(Mesh::get_resource(map_name, false), true);
#endif

	Instance->Map = newMap;
	rMap->SetShader(DEFAULT_SHADER);

#if MAP_BOUNDS_RENDERING
	for (auto &m : Instance->Map->opaque_sub_meshes) {
		m.bounds->EnableRendering();
	}
#endif
	LOG(fmt::format("map {} loaded", map_name), LogType::Message);
}

void Game::Render() {

	float time = glfwGetTime();
	
#if INDIRECT_RENDERING
	IndirectRend::Render();
#else
	for (auto m : Instance->Renderers)
	{
		//todo: Trim renderers from the renderer collection when they get destroyed
		m->Draw();
	}
		/*
	GlDirect_draw prim;
	prim.color3f(1.f, 0.f, 0.f);
	prim.begin(GL_TRIANGLES);
	prim.vertex3f(0.f, 0.f, 0.f);
	prim.vertex3f(0.f, 10.f, 0.f);
	prim.vertex3f(0.f, 0.f, 10.f);
	prim.end();

	// Draw the triangle
	prim.set_matrix(glm::mat4x4(1.0), projection_mat);
	prim.draw();
	// release CPU GPU memory
	prim.clear();
	// Draw nothing
	prim.draw();*/
	
	Renderer::FinishRender();
#endif
}

void Game::Update() {
#if BULLET_PHYSICS
	PhysWorld::step();
#endif

	GameObject::UpdateAll();
}

std::unordered_set<Component*> Game::GetComponents()
{
	return Instance->components;
}
