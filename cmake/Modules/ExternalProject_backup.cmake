set(DEPENDENCY_DEF_DIR "${CMAKE_CONFIG_DIR}/dependency_defs")
set(DEPENDENCY_DOWNLOAD_DIRNAME "download")
set(DEPENDENCY_SRC_DIRNAME "src")
set(DEPENDENCY_BUILD_DIRNAME "build")

function(GenDirs) #Generate the directories used for dependency aquisition
  #set(DEPENDENCY_DIR "${PROJECT_DEPENDENCY_DIR}/${DEPENDENCY_NAME}")
  set(DEPENDENCY_DIR "${PROJECT_DEPENDENCY_DIR}/${DEPENDENCY_NAME}")
  set(DEPENDENCY_DOWNLOAD_DIR "${DEPENDENCY_DIR}/${DEPENDENCY_DOWNLOAD_DIRNAME}" PARENT_SCOPE)
  set(DEPENDENCY_DOWNLOAD_DIR "${DEPENDENCY_DIR}/${DEPENDENCY_DOWNLOAD_DIRNAME}" PARENT_SCOPE)
  set(DEPENDENCY_SRC_DIR "${DEPENDENCY_DIR}/${DEPENDENCY_SRC_DIRNAME}" PARENT_SCOPE)
  set(DEPENDENCY_BUILD_DIR "${DEPENDENCY_DIR}/${DEPENDENCY_BUILD_DIRNAME}" PARENT_SCOPE)

endfunction(GenDirs)

macro(GetExternal name) #Get an external dependency
	set(DEPENDENCY_NAME ${name})
	GenDirs()
	configure_file("${DEPENDENCY_DEF_DIR}/${name}.cmake"  "${DEPENDENCY_DOWNLOAD_DIR}/CMakeLists.txt")

	execute_process(COMMAND ${CMAKE_COMMAND} -G "${CMAKE_GENERATOR}" .
	RESULT_VARIABLE result
	WORKING_DIRECTORY "${DEPENDENCY_DOWNLOAD_DIR}")
	if(result)
		message(FATAL_ERROR "CMake step for ${DEPENDENCY_NAME} failed: ${result}")
	endif()
	execute_process(COMMAND ${CMAKE_COMMAND} --build .
	RESULT_VARIABLE result
	WORKING_DIRECTORY "${DEPENDENCY_DOWNLOAD_DIR}" )
	if(result)
		message(FATAL_ERROR "Build step for ${DEPENDENCY_NAME} failed: ${result}")
	endif()

	add_subdirectory("${DEPENDENCY_SRC_DIR}" 
					 "${DEPENDENCY_BUILD_DIR}")

	include_directories("${DEPENDENCY_SRC_DIR}/include"
						"${DEPENDENCY_SRC_DIR}"
						"${DEPENDENCY_SRC_DIR}/src")

	#set_property(DIRECTORY "${DEPENDENCY_SRC_DIR}" PROPERTY FOLDER "ExternalProjectTargets/${name}")
	#set_property(DIRECTORY "${DEPENDENCY_BUILD_DIR}" PROPERTY FOLDER "ExternalProjectTargets/${name}")
endmacro(GetExternal)

macro(SetExternalDirectories targets)
	foreach(target ${targets})
		set_property(TARGET ${target} PROPERTY FOLDER "Dependencies/${target}")
	endforeach(target)
endmacro(SetExternalDirectories)