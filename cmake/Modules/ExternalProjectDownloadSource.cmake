cmake_minimum_required(VERSION 2.8.12)

set(DEPENDENCY_DOWNLOAD_DIRNAME "download")
set(DEPENDENCY_SRC_DIRNAME "src")
set(DEPENDENCY_BUILD_DIRNAME "build")

function(GenDirs) #Generate the directories used for dependency aquisition
	set(DEPENDENCY_DIR "${PROJECT_DEPENDENCY_DIR}/${DEPENDENCY_NAME}")
	set(DEPENDENCY_DOWNLOAD_DIR "${DEPENDENCY_DIR}/${DEPENDENCY_DOWNLOAD_DIRNAME}" PARENT_SCOPE)
	set(DEPENDENCY_SRC_DIR "${DEPENDENCY_DIR}/${DEPENDENCY_SRC_DIRNAME}" PARENT_SCOPE)
	set(DEPENDENCY_BUILD_DIR "${DEPENDENCY_DIR}/${DEPENDENCY_BUILD_DIRNAME}" PARENT_SCOPE)

endfunction(GenDirs)

macro(SetExternalDirectories targets name)
	set_property(TARGET ${name} PROPERTY FOLDER "Dependencies/DependencyDownloaders")
	#set_target_properties(${name}
	#			PROPERTIES
	#			ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/dependencies"
	#			LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/dependencies"
	#			RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/dependencies"
	#	
	#)
	foreach(target ${targets})
		get_target_property(target_type ${target} TYPE)
		if (NOT target_type STREQUAL "INTERFACE_LIBRARY")
				if(NOT target_type STREQUAL "ALIAS" ) 
				#Get the existing folder prop as to mantain original project structure as much as possible
				get_property(TARGET_FOLDER TARGET ${target} PROPERTY FOLDER)
				STRING(REGEX REPLACE "^(.*)\\_external" "" dummy ${name})
				set_property(TARGET ${target} PROPERTY FOLDER "Dependencies/${CMAKE_MATCH_1}/${TARGET_FOLDER}")
			endif ()
		endif ()
	endforeach(target)
endmacro(SetExternalDirectories)

macro(GetExternal name) #Get an external dependency's source code
	set(DEPENDENCY_NAME ${name})
	GenDirs()
	configure_file("${DEPENDENCY_DEF_DIR}/${name}.cmake"  "${DEPENDENCY_DOWNLOAD_DIR}/CMakeLists.txt")

	execute_process(COMMAND ${CMAKE_COMMAND} -G "${CMAKE_GENERATOR}" .
		RESULT_VARIABLE result
		WORKING_DIRECTORY "${DEPENDENCY_DOWNLOAD_DIR}")
	if(result)
		message(FATAL_ERROR "CMake step for ${DEPENDENCY_NAME} failed: ${result}")
	endif()

	execute_process(COMMAND ${CMAKE_COMMAND} --build .
		RESULT_VARIABLE result
		WORKING_DIRECTORY "${DEPENDENCY_DOWNLOAD_DIR}" )
	if(result)
		message(FATAL_ERROR "Build step for ${DEPENDENCY_NAME} failed: ${result}")
	endif()

	add_subdirectory("${DEPENDENCY_SRC_DIR}"
		"${DEPENDENCY_BUILD_DIR}")

	include("${DEPENDENCY_DEF_DIR}/${name}.cmake")

	include_directories("${DEPENDENCY_SRC_DIR}/include"
		"${DEPENDENCY_SRC_DIR}"
		"${DEPENDENCY_SRC_DIR}/src")

	get_property(DEPENDENCY_TARGETS GLOBAL PROPERTY DEPENDENCY_TARGETS_PROPERTY)
	SetExternalDirectories("${DEPENDENCY_TARGETS}" ${name})
	set_property(GLOBAL PROPERTY DEPENDENCY_TARGETS_PROPERTY "")

endmacro(GetExternal)

