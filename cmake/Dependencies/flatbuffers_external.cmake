cmake_minimum_required(VERSION 2.6)
project(flatbuffers_external NONE)
include(ExternalProject)

#Config flatbuffers, for installation if needed
SET(CMAKE_ARGS
	-DCMAKE_CXX_FLAGS=${CMAKE_CXX_FLAGS}
	-DCMAKE_INSTALL_PREFIX:PATH=<INSTALL_DIR>
	-DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
	-DCMAKE_TOOLCHAIN_FILE=${CMAKE_TOOLCHAIN_FILE}
	-DCMAKE_OSX_ARCHITECTURES=${CMAKE_OSX_ARCHITECTURES}
	-DCMAKE_DEBUG_POSTFIX=_d
	-DINSTALL_LIBS=ON
	#-DUSE_MSVC_RUNTIME_LIBRARY_DLL=ON
	-DBUILD_UNIT_TESTS=OFF
	)

ExternalProject_Add(flatbuffers_external
	UPDATE_DISCONNECTED ${EXTERNAL_UPDATE_DISCONNECT}
	GIT_REPOSITORY		https://github.com/google/flatbuffers.git
	GIT_TAG				master
	PREFIX				${DEPENDENCY_DIR}
	SOURCE_DIR			${DEPENDENCY_SRC_DIR}
	DOWNLOAD_DIR		${DEPENDENCY_DOWNLOAD_DIR}
	BINARY_DIR			${DEPENDENCY_BUILD_DIR}
	CMAKE_ARGS			${CMAKE_ARGS}
	CONFIGURE_COMMAND	""
	BUILD_COMMAND		""
	INSTALL_COMMAND		""
	TEST_COMMAND		""
	#	GIT_PROGRESS TRUE
	)
