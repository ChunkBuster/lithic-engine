﻿# README #
To build, simply run install.sh

Currently, this project is set up to compile with Visual Studio. This will be ported to cmake shortly.
This is a simple game engine, written in pure OpenGL. I am attempting to use as few libraries as possible, except where not doing so would be re-inventing the wheel. 
It is currently capable of yesteryears's AAA graphics, has support for Physics, and has more on the way.

### Project goal roadmap ###

1. Rendering
	- ☑ Basic functionality
		- ☑ GLFW init
		- ☑ Compiling of shaders at runtime
		- ☑ OpenGL rendering
		- ☑ VAO binding
		- ☑ Full cleanup and disposal of glfw/OGL resources
	- ☑ Advanced functionality
		- ☑ Texture mapping
		- ☑ Multiple objects rendered at once
		- ☑ Submeshes stored, rendering with material properties
			- ☑ PBR materials
		- ☑ Frustum culling
			- ☑ Bounding box calculations 
			- ☑ Detection of disjoint submeshes
	- ☑ GPU particles
			- ☑ 7.4 million particles on modern GPUs
			- ☐ Multiple concurrent systems
2. Resource management
	- ☑ Basic resource management
		- ☑ Generic resource access template 
		- ☑ Resource loading and caching
		- ☑ .OBJ and .MTL importing
		- ☑ Full resource disposal
	- ☑ Advanced resource management
		- ☑ Flatbuffers implemented as an intermediary data storage format for faster loads
		- ☑ Automatic sub-resource aquisition from mesh and material data		
3. User I/O
	- ☑ Basic noclip camera
	- ☑ IMGUI implemented for menu functionality
		- ☑ Advanced console output
	- ☐ Physics-based player controller
4. Physics
	- ☑ Bullet physics
		- ☑ Static colliders
		- ☑ Dynamic colliders
		- ☑ Global phys controller
		- ☐ Static collider caching on the disk
5. Gameplay
	- ☐ We'll see if I get here or not
	
###Screenshots:###
![Scheme](images/image0.png)

![Scheme](images/image1.png)

![Scheme](images/image2.png)

![Scheme](images/image3.png)

![Scheme](images/image4.png)

![Scheme](images/image5.png)