#version 420 core
uniform samplerCube u_EnvironmentMap;

smooth in vec3 eyeDirection;

out vec4 fragmentColor;
//#define GAMMA 2.2
#define GAMMA 1.5
vec3 GammaCorrectTextureRGB(samplerCube tex, vec3 dir)
{
	vec4 samp = texture(tex, dir);
	return vec3(pow(samp.rgb, vec3(GAMMA)));
}

void main() {
	//fragmentColor = vec4(0, 0, 0, 1);
	fragmentColor = vec4(GammaCorrectTextureRGB(u_EnvironmentMap, eyeDirection), 1);//texture(u_EnvironmentMap, eyeDirection).rgba;
}