#version 430 core
//This PBR shader was sourced and modified from https://github.com/TheCherno/Sparky

layout(location = 0) in vec4 position;
layout(location = 1) in vec2 uv;
layout(location = 2) in vec3 normal;
layout(location = 3) in vec3 tangent;
layout(location = 4) in vec3 binormal;

flat out int InstanceID; 

uniform mat4 P;
uniform mat4 V;
uniform mat4 M;
uniform mat4 MV;
uniform vec3 _CameraPosition;

uniform mat4 u_DepthBiasMVP;

out DATA
{
	vec4 position;
	vec3 normal;
	vec2 uv;
	vec3 binormal;
	vec3 tangent;
	vec3 color;
	vec4 shadowCoord;
	vec3 cameraPos;
} vs_out;

void main()
{
	
	InstanceID = gl_InstanceID;
	vec4 pos = M * position;
	vs_out.position = pos;
	gl_Position = P * V * pos;
	mat3 model = mat3(M);
	vs_out.normal = model * normal;
	vs_out.binormal = model* binormal;
	vs_out.tangent = model * tangent;
	vs_out.uv = vec2(uv.x, 1 - uv.y);
	vs_out.color = vec3(1.0);
	vs_out.cameraPos = _CameraPosition;
	vs_out.shadowCoord = u_DepthBiasMVP * pos;
}
