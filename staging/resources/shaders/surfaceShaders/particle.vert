#version 430 core
//This PBR shader was sourced and heavily modified from https://github.com/TheCherno/Sparky

#define SPRITES 16
struct particle{
	vec4 pos;
	vec4 timeState;
	vec4 state;
    vec4 velocity;
};

layout(std430, binding = 5) buffer transfBuffer { 
	particle particles[];
};


layout(location = 0) in vec4 position;
layout(location = 1) in vec2 uv;
layout(location = 2) in vec3 normal;
layout(location = 3) in vec3 tangent;
layout(location = 4) in vec3 binormal;

flat out int InstanceID; 

uniform mat4 P;
uniform mat4 V;
uniform mat4 M;
uniform mat4 MV;
uniform vec3 _CameraPosition;

uniform mat4 u_DepthBiasMVP;

out DATA
{
	vec4 position;
	vec3 normal;
	vec2 uv;
	vec3 binormal;
	vec3 tangent;
	vec3 color;
	vec4 shadowCoord;
	vec3 cameraPos;
} vs_out;

void main()
{
	
	vs_out.uv =  uv;
	vs_out.color = vec3(1.0);
	vs_out.cameraPos = _CameraPosition;

	InstanceID = gl_InstanceID;
	particle p = particles[gl_InstanceID];
	mat4 ctModel = mat4(1.0);
	ctModel[3][0] = p.pos.x;
	ctModel[3][1] = p.pos.y;
	ctModel[3][2] = p.pos.z;

  	mat4 modelView = V * ctModel;

	mat3 model = mat3(ctModel);
	vs_out.normal = model * normal;
	vs_out.binormal = model * binormal;
	vs_out.tangent = model * tangent;

	vec3 scale = vec3(p.state.z);
  	// First colunm.
  	modelView[0][0] = 1.0 * scale.x; 
  	modelView[0][1] = 0.0; 
  	modelView[0][2] = 0.0; 
    // Second colunm.
    modelView[1][0] = 0.0; 
    modelView[1][1] = 1.0* scale.y; 
    modelView[1][2] = 0.0; 
  	// Thrid colunm.
  	modelView[2][0] = 0.0; 
  	modelView[2][1] = 0.0; 
  	modelView[2][2] = 1.0* scale.z; 
  
  	vec4 P_out = modelView  * position;
  	gl_Position = P * P_out;
	vs_out.position = gl_Position;	
	vec2 offset = vec2(InstanceID % SPRITES, InstanceID / SPRITES);
	vs_out.uv = (offset + uv) / SPRITES;
	//vs_out.id = int(p.state.w);
}

/*#version 430 core

layout(location = 0) in vec4 position;
layout(location = 1) in vec2 uv;

uniform float u_time;

struct particle{
	vec4 pos;
	vec4 timeState;
	vec4 state;
    vec4 velocity;
};

layout(std430, binding = 5) buffer transfBuffer { 
	particle particles[];
};

flat out int InstanceID; 

uniform mat4 P;
uniform mat4 V;

out PARTICLEDATA
{
	vec4 position;
	vec3 normal;
	vec2 uv;
	//int id;
} vs_out;
#define SPRITES 16
void main()
{
	
	InstanceID = gl_InstanceID;
	particle p = particles[gl_InstanceID];
	mat4 ctModel = mat4(1.0);
	ctModel[3][0] = p.pos.x;
	ctModel[3][1] = p.pos.y;
	ctModel[3][2] = p.pos.z;

  	mat4 modelView = V * ctModel;
	vec3 scale = vec3(p.state.y) * 2;
  	// First colunm.
  	modelView[0][0] = 1.0 * scale.x; 
  	modelView[0][1] = 0.0; 
  	modelView[0][2] = 0.0; 
    // Second colunm.
    modelView[1][0] = 0.0; 
    modelView[1][1] = 1.0* scale.y; 
    modelView[1][2] = 0.0; 
	
  	// Thrid colunm.
  	modelView[2][0] = 0.0; 
  	modelView[2][1] = 0.0; 
  	modelView[2][2] = 1.0* scale.z; 
  
  	vec4 P_out = modelView  * position;
  	gl_Position = P * P_out;
	vs_out.position = gl_Position;	
	vec2 offset = vec2(InstanceID % SPRITES, InstanceID / SPRITES);
	vs_out.uv = (offset + uv) / SPRITES;
	//vs_out.id = int(p.state.w);
}
*/