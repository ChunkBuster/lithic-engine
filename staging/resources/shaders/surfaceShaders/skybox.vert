#version 420 core
/*layout(location = 0)*/  in vec4 aPosition;

uniform mat4 P;
uniform mat4 V;

smooth out vec3 eyeDirection;

void main() {
    mat4 inverseProjection = inverse(P);
    mat3 inverseModelview = transpose(mat3(V));
    vec3 unprojected = (inverseProjection * aPosition).xyz;
    eyeDirection = inverseModelview * unprojected;

    gl_Position = aPosition;
} 